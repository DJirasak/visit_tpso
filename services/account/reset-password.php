<?php
// {
//     "username": "",
//     "curr_password": "",
//     "new_password": ""
// }
if($_SERVER['REQUEST_METHOD']=="POST"){
    $reqJson = json_decode(file_get_contents('php://input'), true);
    require "../controllers/account.php";
    $sample = new Account();
    $resData= $sample->resetPassword($reqJson); 
}else{
    include '../config/missing-method.php';
}
?>
