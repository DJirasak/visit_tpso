<?php
class Account{
    private $conn = null;
    function __construct(){
        include '../config/response.php';
        require "../config/database.php";
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    function generateToken($username, $password){
        return sha1($username).'.'.sha1($password.$_ENV['SALT']);
    }

    function validUsername($username){
        $query = "SELECT * FROM `account` WHERE `username`=:username;";
        try {
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":username",$username,PDO::PARAM_STR);
            $stmt1->execute();
            $rowcount = $stmt1->rowCount();
            if($rowcount >= 1){
                return false;
            }else{
                return true;
            }
        }catch(Exception $err){ 
            return false;
        }
    }

    function createAccount($inpData){
        $sql = "INSERT INTO `account` (";
        $sql .= " `username`, `verify_code`, `role`, `modify_date`";
        $sql .= " )VALUES(";
        $sql .= " :username, :password, :role, NOW());";

        if(!($this->validUsername($inpData['username']))){
            responseJson(400, 'Username Already Exists!');
        }else{
            try{
                $tokenGenerated = $this->generateToken($inpData['username'], $inpData['password']);

                $stmt = $this->conn->prepare($sql);
                $stmt->bindParam(":username", $inpData['username'], PDO::PARAM_STR);
                $stmt->bindParam(":password", $tokenGenerated , PDO::PARAM_STR);
                $nRole = (strtolower($inpData['role'])=='user') ? 1 : 2;
                $stmt->bindParam(":role", $nRole , PDO::PARAM_INT);
                $stmt->execute();
                responseJson(201, 'Account Created');        
            }catch(Exception $err){ 
                responseJson(500, $err->getMessage(), null);
            }
        }
    }

    function auth($inpData){
        $sql = "SELECT * FROM `account` WHERE `username` = :username ;";
        $sqlRole = "SELECT * FROM `role` WHERE `role_id` = :role_id LIMIT 1;";
        try{
            $usnDecode = base64_decode($inpData['username']);
            $pwdDecode = base64_decode($inpData['password']);
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(":username",  $usnDecode, PDO::PARAM_STR);
            $stmt->execute();

            $rowcount = $stmt->rowCount();
            if($rowcount < 1){
                responseJson(404, 'Username Can Not Found!');
            }else{
                $tokenGenerated = $this->generateToken( $usnDecode, $pwdDecode);
                $row = $stmt->fetch();

                $stmt2 = $this->conn->prepare($sqlRole);
                $stmt2->bindParam(":role_id", $row['role'], PDO::PARAM_INT);
                $stmt2->execute();
                $row2 = $stmt2->fetch();

                if($tokenGenerated != $row['verify_code']){
                    responseJson(403, 'Password Incorrect!');
                }else{
                    $resDara = array(
                        "account_id"        =>      $row['account_id'],
                        "username"          =>      $row['username'],
                        "role"              =>      $row2['role']
                    );
                    responseJson(200, 'Authentication successfully!', $resDara);
                }
            }
        }catch(Exception $err){
            responseJson(500, $err->getMessage(), null);
        }
    }

    function resetPassword($inpData){
        $sqlSelect = "SELECT * FROM `account` WHERE `username` = :username LIMIT 1;";
        $sqlUpdate = "UPDATE `account` SET `verify_code`=:vc, `modify_date`=NOW() WHERE `username` = :username ;";

        try{
            $usnDecode = base64_decode($inpData['username']);
            $currPwdDecode = base64_decode($inpData['curr_password']);
            $newPwdDecode = base64_decode($inpData['new_password']);

            $stmt = $this->conn->prepare($sqlSelect);
            $stmt->bindParam(":username", $usnDecode, PDO::PARAM_STR);
            $stmt->execute();
            $row = $stmt->fetch();

            $tokenGenerated = $this->generateToken( $usnDecode, $currPwdDecode);

            if($tokenGenerated != $row['verify_code']){
                responseJson(403, 'Password Incorrect!');
            }else{
                $newTokenGenerated = $this->generateToken( $usnDecode, $newPwdDecode);
                $stmt2 = $this->conn->prepare($sqlUpdate);
                $stmt2->bindParam(":vc", $newTokenGenerated, PDO::PARAM_STR);
                $stmt2->bindParam(":username", $usnDecode, PDO::PARAM_STR);
                $stmt2->execute();

                responseJson(200, 'Update Password successfully!');       
            }  
        }catch(Exception $err){
            responseJson(500, $err->getMessage(), null);
        }

    }

    function __destruct(){
        $this->conn = null;
    }
} 


?>