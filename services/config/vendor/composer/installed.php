<?php return array(
    'root' => array(
        'pretty_version' => 'dev-develop',
        'version' => 'dev-develop',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '7fe9931a8d4fa813b1dce9ea3a8943e68cadccc6',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-develop',
            'version' => 'dev-develop',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '7fe9931a8d4fa813b1dce9ea3a8943e68cadccc6',
            'dev_requirement' => false,
        ),
        'symfony/dotenv' => array(
            'pretty_version' => 'v6.0.2',
            'version' => '6.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/dotenv',
            'aliases' => array(),
            'reference' => '5c43c5515e549a8c2c426be36d40f47daf196968',
            'dev_requirement' => false,
        ),
    ),
);
