var stateDefault = 1
document.getElementById('formSection').style.display = "block"

const gotoSignin = () => {
    window.open('login.php', '_self');
}

const gotoStart = () => {
    window.open('index.php', '_self');
}

const selectRadio = (enbOpt, disOpt1, disOpt2 = null) => {
    document.getElementById(enbOpt).style.display = 'block'
    document.getElementById(disOpt1).style.display = 'none'
    if (disOpt2 != null) {
        document.getElementById(disOpt2).style.display = 'none'

    }
    document.getElementById(enbOpt).name = enbOpt.slice(0, -2);
    document.getElementById(disOpt1).name = '';
}