console.log(localStorage.getItem("objLocal"))
if(localStorage.getItem("objLocal") != 'undefined' && localStorage.getItem("objLocal") != null) {
    let userData = JSON.parse(localStorage.getItem("objLocal"));
    if(userData.role == 'admin' || userData.role == 'user'){
        window.open('./dashboard', '_self')
    }
}

const auth = async() => {
    try{
        showLoading()
        let reqData ={
            username: btoa(document.getElementById('username').value),
            password: btoa(document.getElementById('password').value)
        }
        let result = await axios({
            method: 'post',
            url: './services/account/auth.php',
            data: reqData,
        });
        let objLocal = result.data.data
        localStorage.setItem('objLocal',JSON.stringify(objLocal))
        window.open('./dashboard', '_self')
    }
    catch(err){
        document.getElementById('errLogin').innerHTML = "*รหัสผ่านหรือชื่อผู้ใช้ผิดพลาด กรุณากรอกใหม่"
    }
}