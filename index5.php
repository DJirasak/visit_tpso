<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="assets/logo_icon.png">
    <title>แบบสอบถามการดำเนินงานของศูนย์ช่วยเหลือสังคมตำบล</title>
    <link rel="stylesheet" href="./global/css/style-global.css?version=1.0.2" />
    <link rel="stylesheet" href="./assets/css/style-index.css?version=1.0.6" />
</head>

<body>
    <div class="container">
        <form method="post" action="insert5.php">
            <section id="content">
                <div class="container-login">
                    <div class="login-btn" onclick="gotoStart()">
                        หน้าหลัก
                    </div>
                </div>
                <div class="logo">
                    <img src="./assets/logo.png" />
                </div>
                <div class="title">
                    แบบสอบถามการดำเนินงานของศูนย์ช่วยเหลือสังคมตำบล
                </div>
                <div class="sub-title">
                    (การนิเทศงานของสำนักงานส่งเสริมและสนับสนุนวิชาการ 1-11)
                </div>
                <div class="sec-title" id="secTitle">
                    ส่วนที่ 5/6
                </div>

                <section class="section" id="formSection">
                    <div class="header">
                        ส่วนที่ 5 ด้านการบูรณาการความร่วมมือ
                    </div>
                    <div class="explain">
                        ในส่วนนี้เป็นคำถามที่ต้องการคำตอบในการบูรณาการความร่วมมือในการบริการสวัสดิการสังคมสำหรับคนทุกช่วงวัย
                        โดยร่วมกับองค์กรปกครองส่วนท้องถิ่น ภาครัฐ ภาคประชาสังคม ภาคเอกชน และภาคีเครือข่าย
                        เพื่อการพัฒนาคุณภาพชีวิตกลุ่มเป้าหมายและประชาชนอย่างยั่งยืนในพื้นที่
                    </div>
                    <div class="form">
                        <div class="question">
                            5.1 มีการบูรณาการโครงการ กิจกรรม และงบประมาณร่วมกัน จากไหนบ้าง
                        </div>
                        <div class="answer">
                            <div class="radio-box">
                                <div class="segment">
                                    <input type="radio" id="s5_1_integration_project_true" name="s5_1_integration_project" onchange="selectRadio('s5_1_how_or_because_1', 's5_1_how_or_because_2')" value="มี อย่างไร">
                                    <label for="s5_1_integration_project_true" class="left">มี อย่างไร</label>
                                    <input type="text" class="textInp" id="s5_1_how_or_because_1" placeholder="โปรดระบุ" name="s5_1_how_or_because">
                                </div>
                                <div class="segment">
                                    <input type="radio" id="s5_1_integration_project_false" name="s5_1_integration_project" onchange="selectRadio('s5_1_how_or_because_2', 's5_1_how_or_because_1')" value="ไม่มี เนื่องจาก">
                                    <label for="s5_1_integration_project_false" class="left">ไม่มี เนื่องจาก</label>
                                    <input type="text" class="textInp" id="s5_1_how_or_because_2" placeholder="โปรดระบุ" name="s5_1_how_or_because">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form">
                        <div class="question">
                            5.2 มีการบูรณาการทรัพยากรในการบริหารศูนย์ช่วยเหลือสังคมตำบล อย่างไร
                        </div>
                        <div class="answer">
                            <div class="radio-box">
                                <div class="segment">
                                    <input type="radio" id="s5_2_integration_resource_true" name="s5_2_integration_resource" onchange="selectRadio('s5_2_how_or_because_1', 's5_2_how_or_because_2')" value="มี อย่างไร">
                                    <label for="s5_2_integration_resource_true" class="left">มี อย่างไร </label>
                                    <input type="text" class="textInp" id="s5_2_how_or_because_1" placeholder="โปรดระบุ" name="s5_2_how_or_because">
                                </div>
                                <div class="segment">
                                    <input type="radio" id="s5_2_integration_resource_false" name="s5_2_integration_resource" onchange="selectRadio('s5_2_how_or_because_2', 's5_2_how_or_because_1')" value="ไม่มี เนื่องจาก">
                                    <label for="s5_2_integration_resource_false" class="left">ไม่มี เนื่องจาก</label>
                                    <input type="text" class="textInp" id="s5_2_how_or_because_2" placeholder="โปรดระบุ" name="s5_2_how_or_because">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form">
                        <div class="question">
                            5.3 การมีส่วนร่วมในการทำงานร่วมกับหน่วนงานอื่นในการแก้ไขปัญหาร่วมกัน อย่างไร
                        </div>
                        <div class="answer">
                            <div class="radio-box">
                                <div class="segment">
                                    <input type="radio" id="s5_3_work_together_true" name="s5_3_work_together" onchange="selectRadio('s5_3_how_or_because_1', 's5_3_how_or_because_2')" value="มี อย่างไร">
                                    <label for="s5_3_work_together_true" class="left">มี อย่างไร </label>
                                    <input type="text" class="textInp" id="s5_3_how_or_because_1" placeholder="โปรดระบุ" name="s5_3_how_or_because">
                                </div>
                                <div class="segment">
                                    <input type="radio" id="s5_3_work_together_false" name="s5_3_work_together" onchange="selectRadio('s5_3_how_or_because_2', 's5_3_how_or_because_1')" value="ไม่มี เนื่องจาก">
                                    <label for="s5_3_work_together_false" class="left">ไม่มี เนื่องจาก</label>
                                    <input type="text" class="textInp" id="s5_3_how_or_because_2" placeholder="โปรดระบุ" name="s5_3_how_or_because">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form">
                        <div class="question">
                            5.4 การส่งเสริมความร่วมมือในการจัดบริการสวัสดิการสังคมสำหรับคนทุกช่วงวัยร่วมกับหน่วยงานที่เกี่ยวข้อง
                        </div>
                        <div class="answer">
                            <div class="radio-box">
                                <div class="segment">
                                    <input type="radio" id="s5_4_promote_cooperation_true" name="s5_4_promote_cooperation" onchange="selectRadio('s5_4_how_or_because_1', 's5_4_how_or_because_2')" value="มี อย่างไร">
                                    <label for="s5_4_promote_cooperation_true" class="left">มี อย่างไร </label>
                                    <input type="text" class="textInp" id="s5_4_how_or_because_1" placeholder="โปรดระบุ" name="s5_4_how_or_because">
                                </div>
                                <div class="segment">
                                    <input type="radio" id="s5_4_promote_cooperation_false" name="s5_4_promote_cooperation" onchange="selectRadio('s5_4_how_or_because_2', 's5_4_how_or_because_1')" value="ไม่มี เนื่องจาก">
                                    <label for="s5_4_promote_cooperation_false" class="left">ไม่มี เนื่องจาก</label>
                                    <input type="text" class="textInp" id="s5_4_how_or_because_2" placeholder="โปรดระบุ" name="s5_4_how_or_because">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="container-state" id="container-state">
                    <input type="button" class="next-btn" style="width:15vmax;" value="ย้อนกลับ" onclick="history.back()">
                    <input type="submit" class="next-btn" style="width:15vmax;" value="ถัดไป">
                </div>

            </section>
        </form>

        <script src="./global/js/global.js?version=1.0.2"></script>
        <script src="./assets/js/script-index.js?version=1.0.6"></script>



    </div>
</body>

</html>