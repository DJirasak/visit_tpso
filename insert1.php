<?php
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  require("connectdb.php");

  isset($_POST['s1_3_role']) ? $s1_3_role_in = $_POST['s1_3_role'] : $s1_3_role_in = array();
  if (!empty($s1_3_role_in)) {
    $s1_3_role = "";
    foreach ($s1_3_role_in as $v) {
      $s1_3_role .= $v . ",";
    }
  }

  $s1_2_information = $_POST["s1_2_information"];
  $s1_3_other_role = $_POST["s1_3_other_role"];
  $province = $_POST["province"];
  $amphur = $_POST["amphur"];
  $district = $_POST["district"];

  $sql_insert_sec_1 = "INSERT INTO `section1_general_information` (`s1_2_information`) VALUES (?)";
  $statement = $conn->prepare($sql_insert_sec_1);
  $statement->bind_param('s', $s1_2_information);
  $statement->execute();
  $last_sec1_id = $conn->insert_id;

  $sql_insert_sec_1_3 = "INSERT INTO `section1_3_role` (`s1_3_role`,`s1_3_other_role`,`section1_id`) VALUES (?, ?, ?)";
  $statement3 = $conn->prepare($sql_insert_sec_1_3);
  $s1_3_role = rtrim($s1_3_role, ", ");
  $statement3->bind_param('sss', $s1_3_role, $s1_3_other_role, $last_sec1_id);
  $statement3->execute();

  $sql_insert_form = "INSERT INTO `form` (`province_id`, `amphur_id`, `district_id`, `section1_id`) VALUES (?, ?, ?, ?)";
  $statement_form = $conn->prepare($sql_insert_form);
  $statement_form->bind_param('ssss', $province, $amphur, $district, $last_sec1_id);
  $statement_form->execute();
  $_SESSION["form_id"] = $conn->insert_id;

  header("Location: index2.php");
}
