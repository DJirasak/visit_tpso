<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="assets/logo_icon.png">
    <title>แบบสอบถามการดำเนินงานของศูนย์ช่วยเหลือสังคมตำบล</title>
    <link rel="stylesheet" href="./global/css/style-global.css?version=1.0.2" />
    <link rel="stylesheet" href="./assets/css/style-index.css?version=1.0.6" />
</head>

<body>
    <form method="post" action="insert2.php">
        <section id="content">
            <div class="container-login">
                <div class="login-btn" onclick="gotoStart()">
                    หน้าหลัก
                </div>
            </div>
            <div class="logo">
                <img src="./assets/logo.png" />
            </div>
            <div class="title">
                แบบสอบถามการดำเนินงานของศูนย์ช่วยเหลือสังคมตำบล
            </div>
            <div class="sub-title">
                (การนิเทศงานของสำนักงานส่งเสริมและสนับสนุนวิชาการ 1-11)
            </div>
            <div class="sec-title" id="secTitle">
                ส่วนที่ 2/6
            </div>


            <section class="section" id="formSection">
                <div class="header">
                    ส่วนที่ 2 ด้านกลไกและการบริหารจัดการ
                </div>
                <div class="explain">
                    ในส่วนนี้เป็นคำถามที่ต้องการคำตอบในการขับเคลื่อนการดำเนินงานของศูนย์ช่วยเหลือสังคม อย่างมีประสิทธิภาพ
                    เพื่อการจัดสวัสดิการสังคมให้แก่ประชาชนกลุ่มเป้าหมายได้อย่างทั่วถึงและครอบคลุม ในทุกมิติแบบองค์รวม
                </div>
                <div class="form">
                    <div class="question">
                        2.1 การจัดทำแผนการดำเนินงานของศูนย์ และการดำเนินงานตามแผน
                    </div>
                    <div class="answer">
                        <div class="radio-box">
                            <div class="segment">
                                <input type="radio" id="s2_1_makeplan_operate_true" name="s2_1_makeplan_operate" onchange="selectRadio('s2_1_how_or_because_1', 's2_1_how_or_because_2')" value="มี อย่างไร" required>
                                <label for="s2_1_makeplan_operate_true" class="left">มี อย่างไร</label>
                                <input type="text" class="textInp" id="s2_1_how_or_because_1" placeholder="โปรดระบุ" name="s2_1_how_or_because">
                            </div>
                            <div class="segment">
                                <input type="radio" id="s2_1_makeplan_operate_false" name="s2_1_makeplan_operate" onchange="selectRadio('s2_1_how_or_because_2', 's2_1_how_or_because_1')" value="ไม่มี เนื่องจาก">
                                <label for="s2_1_makeplan_operate_false" class="left">ไม่มี เนื่องจาก</label>
                                <input type="text" class="textInp" id="s2_1_how_or_because_2" placeholder="โปรดระบุ" name="s2_1_how_or_because">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="question">
                        2.2 การใช้ระบบข้อมูลสารสนเทศในการดำเนินงานของศูนย์ เช่น ข้อมูลกลุ่มเปราะบาง ข้อมูลสถานการณ์ในพื้นที่
                        เป้นต้น
                    </div>
                    <div class="answer">
                        <div class="radio-box">
                            <div class="segment">
                                <input type="radio" id="s2_2_use_informationsystem_true" name="s2_2_use_informationsystem" onchange="selectRadio('s2_2_how_or_because_1', 's2_2_how_or_because_2')" value="มี อย่างไร" required>
                                <label for="s2_2_use_informationsystem_true" class="left">มี อย่างไร</label>
                                <input type="text" class="textInp" id="s2_2_how_or_because_1" placeholder="โปรดระบุ" name="s2_2_how_or_because">
                            </div>
                            <div class="segment">
                                <input type="radio" id="s2_2_use_informationsystem_false" name="s2_2_use_informationsystem" onchange="selectRadio('s2_2_how_or_because_2', 's2_2_how_or_because_1')" value="ไม่มี เนื่องจาก">
                                <label for="s2_2_use_informationsystem_false" class="left">ไม่มี เนื่องจาก</label>
                                <input type="text" class="textInp" id="s2_2_how_or_because_2" placeholder="โปรดระบุ" name="s2_2_how_or_because">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="question">
                        2.3 การจัดโครงสร้างการบริหารจัดการภายในศูนย์ฯ (คณะกรรมการ/คณะทำงาน)
                    </div>
                    <div class="answer">
                        <div class="radio-box">
                            <div class="segment">
                                <input type="radio" id="s2_3_structure_management_true" name="s2_3_structure_management" onchange="selectRadio('s2_3_how_or_because_1', 's2_3_how_or_because_2')" value="มี อย่างไร" required>
                                <label for="s2_3_structure_management_true" class="left">มี อย่างไร</label>
                                <input type="text" class="textInp" id="s2_3_how_or_because_1" placeholder="โปรดระบุ" name="s2_3_how_or_because">
                            </div>
                            <div class="segment">
                                <input type="radio" id="s2_3_structure_management_false" name="s2_3_structure_management" onchange="selectRadio('s2_3_how_or_because_2', 's2_3_how_or_because_1')" value="ไม่มี เนื่องจาก">
                                <label for="s2_3_structure_management_false" class="left">ไม่มี เนื่องจาก</label>
                                <input type="text" class="textInp" id="s2_3_how_or_because_2" placeholder="โปรดระบุ" name="s2_3_how_or_because">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="question">
                        2.4 มีบุคลากรให้บริการและจัดกิจกรรมให้แก่ประชาชนกลุ่มเป้าหมายเป็นประจำ
                    </div>
                    <div class="answer">
                        <div class="radio-box">
                            <div class="segment">
                                <input type="radio" id="s2_4_personnel_services_target_true" name="s2_4_personnel_services_target" onchange="selectRadio('s2_4_how_or_because_1', 's2_4_how_or_because_2')" value="มี อย่างไร" required>
                                <label for="s2_4_personnel_services_target_true" class="left">มี อย่างไร</label>
                                <input type="text" class="textInp" id="s2_4_how_or_because_1" placeholder="โปรดระบุ" name="s2_4_how_or_because">
                            </div>
                            <div class="segment">
                                <input type="radio" id="s2_4_personnel_services_target_false" name="s2_4_personnel_services_target" onchange="selectRadio('s2_4_how_or_because_2', 's2_4_how_or_because_1')" value="ไม่มี เนื่องจาก">
                                <label for="s2_4_personnel_services_target_false" class="left">ไม่มี เนื่องจาก</label>
                                <input type="text" class="textInp" id="s2_4_how_or_because_2" placeholder="โปรดระบุ" name="s2_4_how_or_because">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="question">
                        2.5 มีการจัดทำโครงการ/กิจกรรม ขอรับการสนับสนุนจากหน่วยงานที่เกี่ยวข้อง
                    </div>
                    <div class="answer">
                        <div class="radio-box">
                            <div class="segment">
                                <input type="radio" id="s2_5_doproject_getsupport_true" name="s2_5_doproject_getsupport" onchange="selectRadio('s2_5_agencyfrom_or_because_1', 's2_5_agencyfrom_or_because_2')" value="มี หน่วยงานจาก" required>
                                <label for="s2_5_doproject_getsupport_true" class="left">มี หน่วยงานจาก</label>
                                <input type="text" class="textInp" id="s2_5_agencyfrom_or_because_1" placeholder="โปรดระบุ" name="s2_5_agencyfrom_or_because">
                            </div>
                            <div class="segment">
                                <input type="radio" id="s2_5_doproject_getsupport_false" name="s2_5_doproject_getsupport" onchange="selectRadio('s2_5_agencyfrom_or_because_2', 's2_5_agencyfrom_or_because_1')" value="ไม่มี เนื่องจาก">
                                <label for="s2_5_doproject_getsupport_false" class="left">ไม่มี เนื่องจาก</label>
                                <input type="text" class="textInp" id="s2_5_agencyfrom_or_because_2" placeholder="โปรดระบุ" name="s2_5_agencyfrom_or_because">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="question">
                        2.6 การใช้แนวทางการแก้ไขปัญหากลุ่มเปราะบางตามมิติ 5 มิติ ประกอบไปด้วย 1) การเข้าถึงบริการภาครัฐ 2)
                        รายได้ 3) สุขภาพ 4) การศึกษา และ 5) ความเป็นอยู่
                    </div>
                    <div class="answer">
                        <div class="radio-box">
                            <div class="segment">
                                <input type="radio" id="s2_6_fixproblem_5dimention_true" name="s2_6_fixproblem_5dimention" onchange="selectRadio('s2_6_how_or_because_1', 's2_6_how_or_because_2')" value="มี หน่วยงานจาก" required>
                                <label for="s2_6_fixproblem_5dimention_true" class="left">มี หน่วยงานจาก</label>
                                <input type="text" class="textInp" id="s2_6_how_or_because_1" placeholder="โปรดระบุ" name="s2_6_how_or_because">
                            </div>
                            <div class="segment">
                                <input type="radio" id="s2_6_fixproblem_5dimention_false" name="s2_6_fixproblem_5dimention" onchange="selectRadio('s2_6_how_or_because_2', 's2_6_how_or_because_1')" value="ไม่มี เนื่องจาก">
                                <label for="s2_6_fixproblem_5dimention_false" class="left">ไม่มี เนื่องจาก</label>
                                <input type="text" class="textInp" id="s2_6_how_or_because_2" placeholder="โปรดระบุ" name="s2_6_how_or_because">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="question">
                        2.7 มีการสร้างช่องทางการติดต่อในระบบดิจิทัล เช่น Line OA, Group Line เป็นต้น
                    </div>
                    <div class="answer">
                        <div class="radio-box">
                            <div class="segment">
                                <input type="radio" id="s2_7_digital_contact_true" name="s2_7_digital_contact" onchange="selectRadio('s2_7_how_or_because_1', 's2_7_how_or_because_2')" value="มี หน่วยงานจาก" required>
                                <label for="s2_7_digital_contact_true" class="left">มี หน่วยงานจาก</label>
                                <input type="text" class="textInp" id="s2_7_how_or_because_1" placeholder="โปรดระบุ" name="s2_7_how_or_because">
                            </div>
                            <div class="segment">
                                <input type="radio" id="s2_7_digital_contact_false" name="s2_7_digital_contact" onchange="selectRadio('s2_7_how_or_because_2', 's2_7_how_or_because_1')" value="ไม่มี เนื่องจาก">
                                <label for="s2_7_digital_contact_false" class="left">ไม่มี เนื่องจาก</label>
                                <input type="text" class="textInp" id="s2_7_how_or_because_2" placeholder="โปรดระบุ" name="s2_7_how_or_because">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-state" id="container-state">
                <input type="button" class="next-btn" style="width:15vmax;" value="ย้อนกลับ" onclick="history.back()">
                <input type="submit" class="next-btn" style="width:15vmax;" value="ถัดไป">
            </div>

        </section>
    </form>

    <script src="./global/js/global.js?version=1.0.2"></script>
    <script src="./assets/js/script-index.js?version=1.0.6"></script>
</body>

</html>