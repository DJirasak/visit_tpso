<?php
class Account{
    private $conn = null;
    function __construct(){
        include '../config/response.php';
        require "../config/database.php";
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    function generateToken($username, $password){
        return sha1($username).'.'.sha1($password.$_ENV['SALT']);
    }

    function validUsername($username){
        $query = "SELECT * FROM `account` WHERE `username`=:username;";
        try {
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":username",$username,PDO::PARAM_STR);
            $stmt1->execute();
            $rowcount = $stmt1->rowCount();
            if($rowcount >= 1){
                return false;
            }else{
                return true;
            }
        }catch(Exception $err){ 
            return false;
        }
    }

    function createAccount($inpData){
        $sql = "INSERT INTO `account` (";
        $sql .= " `username`, `verify_code`, `role`, `modify_date`";
        $sql .= " )VALUES(";
        $sql .= " :username, :password, :role, NOW());";

        if(!($this->validUsername($inpData['username']))){
            responseJson(400, 'Username Already Exists!');
        }else{
            try{
                $tokenGenerated = $this->generateToken($inpData['username'], $inpData['password']);

                $stmt = $this->conn->prepare($sql);
                $stmt->bindParam(":username", $inpData['username'], PDO::PARAM_STR);
                $stmt->bindParam(":password", $tokenGenerated , PDO::PARAM_STR);
                $nRole = (strtolower($inpData['role'])=='user') ? 1 : 2;
                $stmt->bindParam(":role", $nRole , PDO::PARAM_INT);
                $stmt->execute();
                responseJson(201, 'Account Created');        
            }catch(Exception $err){ 
                responseJson(500, $err->getMessage(), null);
            }
        }
    }


    function __destruct(){
        $this->conn = null;
    }
} 


?>