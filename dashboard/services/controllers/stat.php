<?php
error_reporting(E_ALL ^ E_NOTICE);
class Stat{
    private $conn = null;
    function __construct(){
        include '../config/response.php';
        require "../config/database.php";
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    function getProvince($account_id){
        $sql1 = "SELECT `province` FROM `account_area` WHERE `account_id`=:account_id;";
        $sql2 = "SELECT `PROVINCE_NAME` FROM `province` WHERE `PROVINCE_ID`=:province;";
        try {
            $stmt1 = $this->conn->prepare($sql1);
            $stmt1->bindParam(":account_id", $account_id, PDO::PARAM_STR);
            $stmt1->execute();
            $data = array();
            while ($row = $stmt1->fetch()) {
                $stmt2 = $this->conn->prepare($sql2);
                $stmt2->bindParam(":province", $row['province'], PDO::PARAM_STR);
                $stmt2->execute();
                $row1 = $stmt2->fetch();
                $el = array(
                    "province_id" => $row['province'],
                    "province_name" => trim($row1["PROVINCE_NAME"])
                    
                );
                array_push($data, $el);
            }
            responseJson(200, 'get data success', $data);
            
        } catch (Exception $err){ 
            responseJson(500, $err->getMessage(), null);
        }
    }

    function getStatProvince($account_id){
        $sql0 = "SELECT `role` FROM `account` WHERE `account_id`=:account_id LIMIT 1;";
        $sql1 = "SELECT `province` FROM `account_area` WHERE `account_id`=:account_id;";
        $sql2 = "SELECT * FROM `view_state_province` WHERE `province_id` =:province;";
        $sql3 = "SELECT * FROM `view_state_province`;";
        try {
            $stmt = $this->conn->prepare($sql0);
            $stmt->bindParam(":account_id", $account_id, PDO::PARAM_STR);
            $stmt->execute();
            $row0 = $stmt->fetch();
            if(intval($row0['role'])==2){
                $data = array();
                $stmt3 = $this->conn->prepare($sql3);
                $stmt3->execute();
                while ($row3 = $stmt3->fetch()){
                    $el = array(
                        "province_id" => intval($row3['province_id']),
                        "province_name" => trim($row3["province_name"]),
                        "total" => intval($row3["total"])
                    );
                    array_push($data, $el);
                }
                responseJson(200, 'get data success', $data);
            }else{
                $stmt1 = $this->conn->prepare($sql1);
                $stmt1->bindParam(":account_id", $account_id, PDO::PARAM_STR);
                $stmt1->execute();
                $data = array();
                while ($row = $stmt1->fetch()) {
                    $stmt2 = $this->conn->prepare($sql2);
                    $stmt2->bindParam(":province", $row['province'], PDO::PARAM_STR);
                    $stmt2->execute();
                    $row1 = $stmt2->fetch();
                    $el = array(
                        "province_id" => intval($row1['province_id']),
                        "province_name" => trim($row1["province_name"]),
                        "total" => intval($row1["total"])
                    );
                    array_push($data, $el);
                }
                responseJson(200, 'get data success', $data);
            }
        } catch (Exception $err) {
            responseJson(500, $err->getMessage(), null);
        }
    }

    function getStatDistrict($province_id){
        $sql1 = "SELECT * FROM `view_state_amphur` WHERE `province_id`=:province_id;";
        try {
            $stmt1 = $this->conn->prepare($sql1);
            $stmt1->bindParam(":province_id", $province_id, PDO::PARAM_STR);
            $stmt1->execute();
            $data = array();
            while ($row = $stmt1->fetch()) {
                $el = array(
                    "district_id" => intval($row["amphur_id"]),
                    "district_name" => trim($row["amphur_name"]),
                    "total" => intval($row["total"])
                );
                array_push($data, $el);
            }
            responseJson(200, 'get data success', $data);
        } catch (Exception $err) {
            responseJson(500, $err->getMessage(), null);
        }
    }
    /**
     * Province = จังหวัด
     * District = อำเภอ
     * Sub-District = ตำบล
     */
    function getStatSec1($district_id){
        $sql1 = "SELECT * FROM `view_stat_sec_1` WHERE `AMPHUR_ID`=:district_id LIMIT 1;";
        $sql2 = "SELECT * FROM `view_response` WHERE `AMPHUR_ID`=:district_id;";
        try {
            if ($district_id == null) {
                responseJson(400, 'missing parameter district_id', null);
            } else {
                $data = array(
                    "i_1_1_4"  => array("i_1_1_4_a" => 0, "i_1_1_4_b" => 0),
                    "i_1_2"  => array("i_1_2_a" => 0, "i_1_2_b" => 0, "i_1_2_c" => 0, "i_1_2_d" => 0),
                );

                $stmt1 = $this->conn->prepare($sql1);
                $stmt1->bindParam(":district_id", $district_id, PDO::PARAM_STR);
                $stmt1->execute();
                $row1 = $stmt1->fetch();

                $data["i_1_1_4"]["i_1_1_4_a"] = ($row1['1_1_4_a'] == null) ? 0 : intval($row1['1_1_4_a']);
                $data["i_1_1_4"]["i_1_1_4_b"] = ($row1['1_1_4_b'] == null) ? 0 : intval($row1['1_1_4_b']);


                $stmt2 = $this->conn->prepare($sql2);
                $stmt2->bindParam(":district_id", $district_id, PDO::PARAM_STR);
                $stmt2->execute();

                while ($row2 = $stmt2->fetch()) {
                    if ($row2['s1_3_role'] != null) {
                        $str_arr = preg_split("/\,/", $row2['s1_3_role']);

                        for ($i = 0; $i < count($str_arr); $i++) {
                            if ($str_arr[$i] == "ผู้กำหนดนโยบาย") {
                                $data["i_1_2"]["i_1_2_a"] = intval($data["i_1_2"]["i_1_2_a"]) + 1;
                            }
                            if ($str_arr[$i] == "ผู้ปฏิบัติ") {
                                $data["i_1_2"]["i_1_2_b"] = intval($data["i_1_2"]["i_1_2_b"]) + 1;
                            }
                            if ($str_arr[$i] == "ผู้ประสานงาน") {
                                $data["i_1_2"]["i_1_2_c"] = intval($data["i_1_2"]["i_1_2_c"]) + 1;
                            }
                            if ($str_arr[$i] == "อื่นๆ") {
                                $data["i_1_2"]["i_1_2_d"] = intval($data["i_1_2"]["i_1_2_d"]) + 1;
                            }
                        }
                    }
                }
                responseJson(200, 'get data success', $data);
            }
        } catch (Exception $err) {
            responseJson(500, $err->getMessage(), null);
        }
    }

    function getStatSec2($district_id){
        $sql1 = "SELECT * FROM `view_stat_sec_2` WHERE `AMPHUR_ID`=:district_id;";
        try {
            if($district_id == null){
                responseJson(400, 'missing parameter district_id', null);
            }else{
                $data = array(
                    "i_2_1"  => array("i_2_1_a" => 0, "i_2_1_b" => 0),
                    "i_2_2"  => array("i_2_2_a" => 0, "i_2_2_b" => 0),
                    "i_2_3"  => array("i_2_3_a" => 0, "i_2_3_b" => 0),
                    "i_2_4"  => array("i_2_4_a" => 0, "i_2_4_b" => 0),
                    "i_2_5"  => array("i_2_5_a" => 0, "i_2_5_b" => 0),
                    "i_2_6"  => array("i_2_6_a" => 0, "i_2_6_b" => 0),
                    "i_2_7"  => array("i_2_7_a" => 0, "i_2_7_b" => 0),
                );

                $stmt1 = $this->conn->prepare($sql1);
                $stmt1->bindParam(":district_id", $district_id, PDO::PARAM_STR);
                $stmt1->execute();
                $row1 = $stmt1->fetch();

                $data["i_2_1"]["i_2_1_a"] = ($row1['2_1_y'] == null) ? 0 : intval($row1['2_1_y']);
                $data["i_2_1"]["i_2_1_b"] = ($row1['2_1_n'] == null) ? 0 : intval($row1['2_1_n']);
                $data["i_2_2"]["i_2_2_a"] = ($row1['2_2_y'] == null) ? 0 : intval($row1['2_2_y']);
                $data["i_2_2"]["i_2_2_b"] = ($row1['2_2_n'] == null) ? 0 : intval($row1['2_2_n']);
                $data["i_2_3"]["i_2_3_a"] = ($row1['2_3_y'] == null) ? 0 : intval($row1['2_3_y']);
                $data["i_2_3"]["i_2_3_b"] = ($row1['2_3_n'] == null) ? 0 : intval($row1['2_3_n']);
                $data["i_2_4"]["i_2_4_a"] = ($row1['2_4_y'] == null) ? 0 : intval($row1['2_4_y']);
                $data["i_2_4"]["i_2_4_b"] = ($row1['2_4_n'] == null) ? 0 : intval($row1['2_4_n']);
                $data["i_2_5"]["i_2_5_a"] = ($row1['2_5_y'] == null) ? 0 : intval($row1['2_5_y']);
                $data["i_2_5"]["i_2_5_b"] = ($row1['2_5_n'] == null) ? 0 : intval($row1['2_5_n']);
                $data["i_2_6"]["i_2_6_a"] = ($row1['2_6_y'] == null) ? 0 : intval($row1['2_6_y']);
                $data["i_2_6"]["i_2_6_b"] = ($row1['2_6_n'] == null) ? 0 : intval($row1['2_6_n']);
                $data["i_2_7"]["i_2_7_a"] = ($row1['2_7_y'] == null) ? 0 : intval($row1['2_7_y']);
                $data["i_2_7"]["i_2_7_b"] = ($row1['2_7_n'] == null) ? 0 : intval($row1['2_7_n']);

                responseJson(200, 'get data success', $data);
            }
        } catch (Exception $err) {
            responseJson(500, $err->getMessage(), null);
        }
    }

    function getStatSec3($district_id){
        $sql1 = "SELECT * FROM `view_stat_sec_3` WHERE `AMPHUR_ID`=:district_id;";
        try {
            if ($district_id == null) {
                responseJson(400, 'missing parameter district_id', null);
            } else {
                $data = array(
                    "i_3_1"  => array("i_3_1_a" => 0, "i_3_1_b" => 0),
                    "i_3_2"  => array("i_3_2_a" => 0, "i_3_2_b" => 0),
                    "i_3_3"  => array("i_3_3_a" => 0, "i_3_3_b" => 0),
                    "i_3_4"  => array("i_3_4_a" => 0, "i_3_4_b" => 0),
                );
                $stmt1 = $this->conn->prepare($sql1);
                $stmt1->bindParam(":district_id", $district_id, PDO::PARAM_STR);
                $stmt1->execute();
                $row1 = $stmt1->fetch();

                $data["i_3_1"]["i_3_1_a"] = ($row1['3_1_y'] == null) ? 0 : intval($row1['3_1_y']);
                $data["i_3_1"]["i_3_1_b"] = ($row1['3_1_n'] == null) ? 0 : intval($row1['3_1_n']);
                $data["i_3_2"]["i_3_2_a"] = ($row1['3_2_y'] == null) ? 0 : intval($row1['3_2_y']);
                $data["i_3_2"]["i_3_2_b"] = ($row1['3_2_n'] == null) ? 0 : intval($row1['3_2_n']);
                $data["i_3_3"]["i_3_3_a"] = ($row1['3_3_y'] == null) ? 0 : intval($row1['3_3_y']);
                $data["i_3_3"]["i_3_3_b"] = ($row1['3_3_n'] == null) ? 0 : intval($row1['3_3_n']);
                $data["i_3_4"]["i_3_4_a"] = ($row1['3_4_y'] == null) ? 0 : intval($row1['3_4_y']);
                $data["i_3_4"]["i_3_4_b"] = ($row1['3_4_n'] == null) ? 0 : intval($row1['3_4_n']);

                responseJson(200, 'get data success', $data);
            }
        } catch (Exception $err) {
            responseJson(500, $err->getMessage(), null);
        }
    }
    
    function getStatSec4($district_id){
        $sql1 = "SELECT * FROM `view_stat_sec_4` WHERE `AMPHUR_ID`=:district_id;";
        try {
            if ($district_id == null) {
                responseJson(400, 'missing parameter district_id', null);
            } else {
                $data = array(
                    "i_4_1"  => array("i_4_1_a" => 0, "i_4_1_b" => 0),
                    "i_4_2"  => array("i_4_2_a" => 0, "i_4_2_b" => 0, "i_4_2_c" => 0),
                    "i_4_3"  => array("i_4_3_a" => 0, "i_4_3_b" => 0),
                    "i_4_4"  => array("i_4_4_a" => 0, "i_4_4_b" => 0),
                    "i_4_5"  => array("i_4_5_a" => 0, "i_4_5_b" => 0),
                    "i_4_6"  => array("i_4_6_a" => 0, "i_4_6_b" => 0),
                );
                $stmt1 = $this->conn->prepare($sql1);
                $stmt1->bindParam(":district_id", $district_id, PDO::PARAM_STR);
                $stmt1->execute();
                $row1 = $stmt1->fetch();
                
                $data["i_4_1"]["i_4_1_a"] = ($row1['4_1_y'] == null) ? 0 : intval($row1['4_1_y']);
                $data["i_4_1"]["i_4_1_b"] = ($row1['4_1_n'] == null) ? 0 : intval($row1['4_1_n']);
                $data["i_4_2"]["i_4_2_a"] = ($row1['4_2_y'] == null) ? 0 : intval($row1['4_2_y']);
                $data["i_4_2"]["i_4_2_b"] = ($row1['4_2_n'] == null) ? 0 : intval($row1['4_2_n']);
                $data["i_4_2"]["i_4_2_c"] = ($row1['4_2_issue'] == null) ? 0 : intval($row1['4_2_issue']);
                $data["i_4_3"]["i_4_3_b"] = ($row1['4_3_n'] == null) ? 0 : intval($row1['4_3_n']);
                $data["i_4_3"]["i_4_3_a"] = ($row1['4_3_y'] == null) ? 0 : intval($row1['4_4_y']);
                $data["i_4_4"]["i_4_4_b"] = ($row1['4_4_n'] == null) ? 0 : intval($row1['4_4_n']);
                $data["i_4_4"]["i_4_4_a"] = ($row1['4_4_y'] == null) ? 0 : intval($row1['4_4_y']);
                $data["i_4_5"]["i_4_5_b"] = ($row1['4_5_n'] == null) ? 0 : intval($row1['4_4_n']);
                $data["i_4_5"]["i_4_5_a"] = ($row1['4_5_y'] == null) ? 0 : intval($row1['4_4_y']);
                $data["i_4_6"]["i_4_6_b"] = ($row1['4_6_n'] == null) ? 0 : intval($row1['4_4_n']);
                $data["i_4_6"]["i_4_6_a"] = ($row1['4_6_y'] == null) ? 0 : intval($row1['4_4_y']);

                responseJson(200, 'get data success', $data);
            }
        } catch (Exception $err) {
            responseJson(500, $err->getMessage(), null);
        }
    }

    function getStatSec5($district_id){
        $sql1 = "SELECT * FROM `view_stat_sec_5` WHERE `AMPHUR_ID`=:district_id;";
        try {
            if ($district_id == null) {
                responseJson(400, 'missing parameter district_id', null);
            } else {
                $data = array(
                    "i_5_1"  => array("i_5_1_a" => 0, "i_5_1_b" => 0),
                    "i_5_2"  => array("i_5_2_a" => 0, "i_5_2_b" => 0),
                    "i_5_3"  => array("i_5_3_a" => 0, "i_5_3_b" => 0),
                    "i_5_4"  => array("i_5_4_a" => 0, "i_5_4_b" => 0),
                );
                $stmt1 = $this->conn->prepare($sql1);
                $stmt1->bindParam(":district_id", $district_id, PDO::PARAM_STR);
                $stmt1->execute();
                $row1 = $stmt1->fetch();

                $data["i_5_1"]["i_5_1_a"] = ($row1['5_1_y'] == null) ? 0 : intval($row1['5_1_y']);
                $data["i_5_1"]["i_5_1_b"] = ($row1['5_1_n'] == null) ? 0 : intval($row1['5_1_n']);
                $data["i_5_2"]["i_5_2_a"] = ($row1['5_2_y'] == null) ? 0 : intval($row1['5_2_y']);
                $data["i_5_2"]["i_5_2_b"] = ($row1['5_2_n'] == null) ? 0 : intval($row1['5_2_n']);
                $data["i_5_3"]["i_5_3_a"] = ($row1['5_3_y'] == null) ? 0 : intval($row1['5_3_y']);
                $data["i_5_3"]["i_5_3_b"] = ($row1['5_3_n'] == null) ? 0 : intval($row1['5_3_n']);
                $data["i_5_4"]["i_5_4_a"] = ($row1['5_4_y'] == null) ? 0 : intval($row1['5_4_y']);
                $data["i_5_4"]["i_5_4_b"] = ($row1['5_4_n'] == null) ? 0 : intval($row1['5_4_n']);

                responseJson(200, 'get data success', $data);
            }
        } catch (Exception $err) {
            responseJson(500, $err->getMessage(), null);
        }
    }
    function getStatDetail($district_id, $item){
        try {
            if ($district_id == null) {
                responseJson(400, 'missing parameter district_id', null);
            } else {
                $itemMap = array(
                    //* sec1
                    "i_1_2_d"       => "s1_3_other_role",
                    //* sec2
                    "i_2_1_a"       => array("key"=>"มี%", "note"=>"s2_1_how_or_because", "col"=>"s2_1_makeplan_operate"),
                    "i_2_1_b"       => array("key"=>"ไม่%", "note"=>"s2_1_how_or_because", "col"=>"s2_1_makeplan_operate"),
                    "i_2_2_a"       => array("key"=>"มี%", "note"=>"s2_2_how_or_because", "col"=>"s2_2_use_informationsystem"),
                    "i_2_2_b"       => array("key"=>"ไม่มี%", "note"=>"s2_2_how_or_because", "col"=>"s2_2_use_informationsystem"),
                    "i_2_3_a"       => array("key"=>"มี%", "note"=>"s2_3_how_or_because", "col"=>"s2_3_structure_management"),
                    "i_2_3_b"       => array("key"=>"ไม่มี%", "note"=>"s2_3_how_or_because", "col"=>"s2_3_structure_management"),
                    "i_2_4_a"       => array("key"=>"มี%", "note"=>"s2_4_how_or_because", "col"=>"s2_4_personnel_services_target"),
                    "i_2_4_b"       => array("key"=>"ไม่มี%", "note"=>"s2_4_how_or_because", "col"=>"s2_4_personnel_services_target"),
                    "i_2_5_a"       => array("key"=>"มี%", "note"=>"s2_5_agencyfrom_or_because", "col"=>"s2_5_doproject_getsupport"),
                    "i_2_5_b"       => array("key"=>"ไม่มี%", "note"=>"s2_5_agencyfrom_or_because", "col"=>"s2_5_doproject_getsupport"),
                    "i_2_6_a"       => array("key"=>"มี%", "note"=>"s2_6_how_or_because", "col"=>"s2_6_fixproblem_5dimention"),
                    "i_2_6_b"       => array("key"=>"ไม่มี%", "note"=>"s2_6_how_or_because", "col"=>"s2_6_fixproblem_5dimention"),
                    "i_2_7_a"       => array("key"=>"มี%", "note"=>"s2_7_how_or_because", "col"=>"s2_7_digital_contact"),
                    "i_2_7_b"       => array("key"=>"ไม่มี%", "note"=>"s2_7_how_or_because", "col"=>"s2_7_digital_contact"),
                    //* sec3
                    "i_3_1_a"       => array("key"=>"เหมาะสม%", "note"=>"s3_1_good_bad_place_because", "col"=>"s3_1_place_check"),
                    "i_3_1_b"       => array("key"=>"ไม่เหมาะสม%", "note"=>"s3_1_good_bad_place_because", "col"=>"s3_1_place_check"),
                    "i_3_2_a"       => array("key"=>"เพียงพอ%", "note"=>"s3_2_good_bad_area_check", "col"=>"s3_2_area_check"),
                    "i_3_2_b"       => array("key"=>"ไม่เพียงพอ%", "note"=>"s3_2_good_bad_area_check", "col"=>"s3_2_area_check"),
                    "i_3_3_a"       => array("key"=>"เพียงพอ%", "note"=>"s3_3_good_bad_toilet_check", "col"=>"s3_3_toilet_check"),
                    "i_3_3_b"       => array("key"=>"ไม่เพียงพอ%", "note"=>"s3_3_good_bad_toilet_check", "col"=>"s3_3_toilet_check"),
                    "i_3_4_a"       => array("key"=>"เพียงพอ%", "note"=>"s3_4_good_bad_facility_because", "col"=>"s3_4_facility_check"),
                    "i_3_4_b"       => array("key"=>"ไม่เพียงพอ%", "note"=>"s3_4_good_bad_facility_because", "col"=>"s3_4_facility_check"),
                    //* sec4
                    "i_4_1_a"       => array("key"=>"มี%", "note"=>"s4_1_how_or_because", "col"=>"s4_1_tpmap_conclude"),
                    "i_4_1_b"       => array("key"=>"ไม่%", "note"=>"s4_1_how_or_because", "col"=>"s4_1_tpmap_conclude"),
                    "i_4_2_a"       => array("key"=>"มี%", "note"=>"s4_2_how_or_because", "col"=>"s4_2_tpmap_analyze"),
                    "i_4_2_b"       => array("key"=>"ไม่%", "note"=>"s4_2_how_or_because", "col"=>"s4_2_tpmap_analyze"),
                    "i_4_2_c"       => array("key"=>"ประเด็น%", "note"=>"s4_2_how_or_because", "col"=>"s4_2_tpmap_analyze"),
                    "i_4_3_a"       => array("key"=>"มี%", "note"=>"s4_3_how_or_because", "col"=>"s4_3_tpmap_plan"),
                    "i_4_3_b"       => array("key"=>"ไม่%", "note"=>"s4_3_how_or_because", "col"=>"s4_3_tpmap_plan"),
                    "i_4_4_a"       => array("key"=>"มี%", "note"=>"s4_4_how_or_because", "col"=>"s4_4_tpmap_protect"),
                    "i_4_4_b"       => array("key"=>"ไม่%", "note"=>"s4_4_how_or_because", "col"=>"s4_4_tpmap_protect"),
                    "i_4_5_a"       => array("key"=>"ทั่วถึง%", "note"=>"s4_5_no_thorough_because", "col"=>"s4_5_thorough_service"),
                    "i_4_5_b"       => array("key"=>"ไม่ทั่วถึง%", "note"=>"s4_5_no_thorough_because", "col"=>"s4_5_thorough_service"),
                    "i_4_6_a"       => array("key"=>"ตรง%", "note"=>"s4_6_goal_bad_because", "col"=>"s4_6_goal_service"),
                    "i_4_6_b"       => array("key"=>"ไม่ตรง%", "note"=>"s4_6_goal_bad_because", "col"=>"s4_6_goal_service"),
                    "i_4_7_a"       => "s4_7_1_access_service",
                    "i_4_7_b"       => "s4_7_2_income",
                    "i_4_7_c"       => "s4_7_3_healt",
                    "i_4_7_d"       => "s4_7_4_education",
                    "i_4_7_e"       => "s4_7_5_being",
                    //* sec5
                    "i_5_1_a"       => array("key"=>"มี%", "note"=>"s5_1_how_or_because", "col"=>"s5_1_integration_project"),
                    "i_5_1_b"       => array("key"=>"ไม่%", "note"=>"s5_1_how_or_because", "col"=>"s5_1_integration_project"),
                    "i_5_2_a"       => array("key"=>"มี%", "note"=>"s5_2_how_or_because", "col"=>"s5_2_integration_resource"),
                    "i_5_2_b"       => array("key"=>"ไม่%", "note"=>"s5_2_how_or_because", "col"=>"s5_2_integration_resource"),
                    "i_5_3_a"       => array("key"=>"มี%", "note"=>"s5_3_how_or_because", "col"=>"s5_3_work_together"),
                    "i_5_3_b"       => array("key"=>"ไม่%", "note"=>"s5_3_how_or_because", "col"=>"s5_3_work_together"),
                    "i_5_4_a"       => array("key"=>"มี%", "note"=>"s5_4_how_or_because", "col"=>"s5_4_promote_cooperation"),
                    "i_5_4_b"       => array("key"=>"ไม่%", "note"=>"s5_4_how_or_because", "col"=>"s5_4_promote_cooperation"),
                    //* sec6
                    "i_6_1_a"       => "s6_1_1_problem_management",
                    "i_6_1_b"       => "s6_1_2_problem_place",
                    "i_6_1_c"       => "s6_1_3_problem_service",
                    "i_6_1_d"       => "s6_1_4_problem_integration",
                    "i_6_2_a"       => "s6_2_1_offer_management",
                    "i_6_2_b"       => "s6_2_2_offer_place",
                    "i_6_2_c"       => "s6_2_3_offer_service",
                    "i_6_2_d"       => "s6_2_4_offer_management",
                );


                $data = array();

                if(
                    ($item=="i_1_2_d") ||
                    ($item=="i_4_7_a") ||
                    ($item=="i_4_7_b") ||
                    ($item=="i_4_7_c") ||
                    ($item=="i_4_7_d") ||
                    ($item=="i_4_7_e") ||
                    ($item=="i_6_1_a") ||
                    ($item=="i_6_1_b") ||
                    ($item=="i_6_1_c") ||
                    ($item=="i_6_1_d") ||
                    ($item=="i_6_2_a") ||
                    ($item=="i_6_2_b") ||
                    ($item=="i_6_2_c") ||
                    ($item=="i_6_2_d") 

                ){
                    $sql = "SELECT `$itemMap[$item]` FROM `view_response`  WHERE `AMPHUR_ID` = :district_id; ";
                
                    $stmt1 = $this->conn->prepare($sql);
                    $stmt1->bindParam(":district_id", $district_id, PDO::PARAM_STR);
                    $stmt1->execute();

                    while($row = $stmt1->fetch()){
                        if($row[$itemMap[$item]] != null && $row[$itemMap[$item]] != ""){
                            array_push($data,$row[$itemMap[$item]]); 
                        }
                    }
                }else{
                    $con = $itemMap[$item]['col'];
                    $conVal = $itemMap[$item]['key'];
                    $condition = "`$con` LIKE '$conVal'";
                    $selectCol = $itemMap[$item]['note'];

                    $sql = "SELECT `$selectCol` FROM `view_response`  WHERE `AMPHUR_ID` = :district_id AND  $condition ;";
                
                    $stmt1 = $this->conn->prepare($sql);
                    $stmt1->bindParam(":district_id", $district_id, PDO::PARAM_STR);
                    $stmt1->execute();
                    while($row = $stmt1->fetch()){
                        if($row[$itemMap[$item]['note']] != null && $row[$itemMap[$item]['note']] != ""){
                            array_push($data,$row[$itemMap[$item]['note']]); 
                        }
                    }
                }

                responseJson(200, 'get data success', $data);
            }
        } catch (Exception $err) {
            responseJson(500, $err->getMessage(), null);
        }
    }
    function __destruct(){
        $this->conn = null;
    }
}