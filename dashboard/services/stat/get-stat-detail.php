<?php
if ($_SERVER['REQUEST_METHOD'] == "GET") {
    // $reqJson = json_decode(file_get_contents('php://input'), true);
    require "../controllers/stat.php";
    $sample = new Stat();
    $resData = $sample->getStatDetail($_GET["district_id"], $_GET["i"]);
} else {
    include '../config/missing-method.php';
}