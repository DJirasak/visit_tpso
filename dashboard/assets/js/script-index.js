var tableState = 1;
var stateSelect ;
var myChart;
let userData = JSON.parse(localStorage.getItem("objLocal"));

if(userData == null){
  window.open('../','_self')
}
else{
  if(userData.role == 'admin' || userData.role == 'user'){
    document.getElementById('usernameShow').innerHTML = userData.username;
  }
  else{
      window.open('../','_self')
  }
}

const getProvinceTable = async()=> {
  document.getElementById('district-sec3').innerHTML = ''
  try{
      showLoading()
      let result = await axios.get('./services/stat/get-stat-province.php', {params: {account_id:userData.account_id}});
      let resData = result.data.data;
      let tableProvince = document.getElementById('td-province')
      let strTable = ''
      for(let i=0;i<resData.length;i++){
        strTable += `
        <div class="td" onclick="getDistrictTable('${resData[i].province_name}','${resData[i].province_id}')">
          <div class="number">${i+1}</div>
          <div class="province">${resData[i].province_name}</div>
          <div class="amount">${resData[i].total} &nbsp; คน</div>
        </div>
        `
      }
      tableProvince.innerHTML = strTable
      hideLoading()
  }
  catch(err){
      // console.log(err)
  }
}

const getDistrictTable = async(province,id)=>{
  document.getElementById('district-sec3').innerHTML = 'จังหวัด' + province + '&nbsp;'
  tableState = tableState + 1
  if(tableState == 2){
    document.getElementById('table1').style.display = 'none'
    document.getElementById('back-btn').style.display = 'block'
    document.getElementById('table2').style.display = 'block'
    document.getElementById('province-name').innerHTML = 'รายชื่ออำเภอ ของจังหวัด' + province

    try{
      showLoading()
      let result = await axios.get('./services/stat/get-stat-district.php', {params: {province_id:id}});
      let resData = result.data.data;
      let tableDistrict = document.getElementById('td-district')
      let strTable = ''
      for(let i=0;i<resData.length;i++){
        strTable += `
        <div class="td" onclick="getChoiceStats('${resData[i].district_name}','${resData[i].district_id}')">
          <div class="number">${i+1}</div>
          <div class="province">${resData[i].district_name}</div>
          <div class="amount">${resData[i].total} &nbsp; คน</div>
        </div>
        `
      }
      tableDistrict.innerHTML = strTable
      hideLoading()
    }
    catch(err){
        // console.log(err)
    }
  }
  // console.log(province)
}

const getChoiceStats = async(name,id)=>{
  document.getElementById('district-sec3').innerHTML += 'อำเภอ' + name
  tableState = tableState + 1
  if(tableState == 3){
    document.getElementById('table1').style.display = 'none'
    document.getElementById('back-btn').style.display = 'block'
    document.getElementById('table2').style.display = 'none'
    document.getElementById('answer').style.display = 'block'

    try{
      showLoading()
      
      let result1 = await axios.get('./services/stat/get-stat-sec-1.php', {params: {district_id:id}});
      let result2 = await axios.get('./services/stat/get-stat-sec-2.php', {params: {district_id:id}});
      let result3 = await axios.get('./services/stat/get-stat-sec-3.php', {params: {district_id:id}});
      let result4 = await axios.get('./services/stat/get-stat-sec-4.php', {params: {district_id:id}});
      let result5 = await axios.get('./services/stat/get-stat-sec-5.php', {params: {district_id:id}});
      let resData1 = result1.data.data;
      let resData2 = result2.data.data;
      let resData3 = result3.data.data;
      let resData4 = result4.data.data;
      let resData5 = result5.data.data; 

      // Section 1
      document.getElementById('i_1_1_4_a').innerHTML = resData1.i_1_1_4.i_1_1_4_a + '&nbsp;คน'
      document.getElementById('i_1_1_4_b').innerHTML = resData1.i_1_1_4.i_1_1_4_b + '&nbsp;คน'
      document.getElementById('i_1_2_a').innerHTML = resData1.i_1_2.i_1_2_a + '&nbsp;คน'
      document.getElementById('i_1_2_b').innerHTML = resData1.i_1_2.i_1_2_b + '&nbsp;คน'
      document.getElementById('i_1_2_c').innerHTML = resData1.i_1_2.i_1_2_c + '&nbsp;คน'
      document.getElementById('i_1_2_d').innerHTML = resData1.i_1_2.i_1_2_d + '&nbsp;คน'
      // Section 2
      document.getElementById('i_2_1_a').innerHTML = resData2.i_2_1.i_2_1_a + '&nbsp;คน'
      document.getElementById('i_2_1_b').innerHTML = resData2.i_2_1.i_2_1_b + '&nbsp;คน'
      document.getElementById('i_2_2_a').innerHTML = resData2.i_2_2.i_2_2_a + '&nbsp;คน'
      document.getElementById('i_2_2_b').innerHTML = resData2.i_2_2.i_2_2_b + '&nbsp;คน'
      document.getElementById('i_2_3_a').innerHTML = resData2.i_2_3.i_2_3_a + '&nbsp;คน'
      document.getElementById('i_2_3_b').innerHTML = resData2.i_2_3.i_2_3_b + '&nbsp;คน'
      document.getElementById('i_2_4_a').innerHTML = resData2.i_2_4.i_2_4_a + '&nbsp;คน'
      document.getElementById('i_2_4_b').innerHTML = resData2.i_2_4.i_2_4_b + '&nbsp;คน'
      document.getElementById('i_2_5_a').innerHTML = resData2.i_2_5.i_2_5_a + '&nbsp;คน'
      document.getElementById('i_2_5_b').innerHTML = resData2.i_2_5.i_2_5_b + '&nbsp;คน'
      document.getElementById('i_2_6_a').innerHTML = resData2.i_2_6.i_2_6_a + '&nbsp;คน'
      document.getElementById('i_2_6_b').innerHTML = resData2.i_2_6.i_2_6_b + '&nbsp;คน'
      document.getElementById('i_2_7_a').innerHTML = resData2.i_2_7.i_2_7_a + '&nbsp;คน'
      document.getElementById('i_2_7_b').innerHTML = resData2.i_2_7.i_2_7_b + '&nbsp;คน'
      // Section 3
      document.getElementById('i_3_1_a').innerHTML = resData3.i_3_1.i_3_1_a + '&nbsp;คน'
      document.getElementById('i_3_1_b').innerHTML = resData3.i_3_1.i_3_1_b + '&nbsp;คน'
      document.getElementById('i_3_2_a').innerHTML = resData3.i_3_2.i_3_2_a + '&nbsp;คน'
      document.getElementById('i_3_2_b').innerHTML = resData3.i_3_2.i_3_2_b + '&nbsp;คน'
      document.getElementById('i_3_3_a').innerHTML = resData3.i_3_3.i_3_3_a + '&nbsp;คน'
      document.getElementById('i_3_3_b').innerHTML = resData3.i_3_3.i_3_3_b + '&nbsp;คน'
      document.getElementById('i_3_4_a').innerHTML = resData3.i_3_4.i_3_4_a + '&nbsp;คน'
      document.getElementById('i_3_4_b').innerHTML = resData3.i_3_4.i_3_4_b + '&nbsp;คน'
      // Section 4
      document.getElementById('i_4_1_a').innerHTML = resData4.i_4_1.i_4_1_a + '&nbsp;คน'
      document.getElementById('i_4_1_b').innerHTML = resData4.i_4_1.i_4_1_b + '&nbsp;คน'
      document.getElementById('i_4_2_a').innerHTML = resData4.i_4_2.i_4_2_a + '&nbsp;คน'
      document.getElementById('i_4_2_b').innerHTML = resData4.i_4_2.i_4_2_b + '&nbsp;คน'
      document.getElementById('i_4_2_c').innerHTML = resData4.i_4_2.i_4_2_c + '&nbsp;คน'
      document.getElementById('i_4_3_a').innerHTML = resData4.i_4_3.i_4_3_a + '&nbsp;คน'
      document.getElementById('i_4_3_b').innerHTML = resData4.i_4_3.i_4_3_b + '&nbsp;คน'
      document.getElementById('i_4_4_a').innerHTML = resData4.i_4_4.i_4_4_a + '&nbsp;คน'
      document.getElementById('i_4_4_b').innerHTML = resData4.i_4_4.i_4_4_b + '&nbsp;คน'
      document.getElementById('i_4_5_a').innerHTML = resData4.i_4_5.i_4_5_a + '&nbsp;คน'
      document.getElementById('i_4_5_b').innerHTML = resData4.i_4_5.i_4_5_b + '&nbsp;คน'
      document.getElementById('i_4_6_a').innerHTML = resData4.i_4_6.i_4_6_a + '&nbsp;คน'
      document.getElementById('i_4_6_b').innerHTML = resData4.i_4_6.i_4_6_b + '&nbsp;คน'
      // Section 5
      document.getElementById('i_5_1_a').innerHTML = resData5.i_5_1.i_5_1_a + '&nbsp;คน'
      document.getElementById('i_5_1_b').innerHTML = resData5.i_5_1.i_5_1_b + '&nbsp;คน'
      document.getElementById('i_5_2_a').innerHTML = resData5.i_5_2.i_5_2_a + '&nbsp;คน'
      document.getElementById('i_5_2_b').innerHTML = resData5.i_5_2.i_5_2_b + '&nbsp;คน'
      document.getElementById('i_5_3_a').innerHTML = resData5.i_5_3.i_5_3_a + '&nbsp;คน'
      document.getElementById('i_5_3_b').innerHTML = resData5.i_5_3.i_5_3_b + '&nbsp;คน'
      document.getElementById('i_5_4_a').innerHTML = resData5.i_5_4.i_5_4_a + '&nbsp;คน'
      document.getElementById('i_5_4_b').innerHTML = resData5.i_5_4.i_5_4_b + '&nbsp;คน'

      document.getElementById('district-id-hide').value = id
      hideLoading()
    }
    catch(err){
        // console.log(err)
    }
  }
}

const backState =()=>{
  tableState = tableState - 1
  if(tableState == 1){
    document.getElementById('table1').style.display = 'block'
    document.getElementById('back-btn').style.display = 'none'
    document.getElementById('table2').style.display = 'none'
    document.getElementById('answer').style.display = 'none'
  }
  if(tableState == 2){
    document.getElementById('district-sec3').innerHTML = ''
    document.getElementById('table1').style.display = 'none'
    document.getElementById('back-btn').style.display = 'block'
    document.getElementById('table2').style.display = 'block'
    document.getElementById('answer').style.display = 'none'
  }
}

const gotoSignIn =()=> {
    localStorage.removeItem('objLocal');
    window.open('../','_self')
}

const changePassword =()=>{
  window.open('../change-password','_self')
}

const hideModal =()=>{
  document.getElementById('modalDetails').style.display = "none"
}

const showModal = async(num)=>{
  showLoading()
  document.getElementById('modalDetails').style.display = "block"
  let districtId = document.getElementById('district-id-hide').value
  
  try{
    let objData = {
      district_id: districtId,
      i: num
    }
    let result = await axios.get('./services/stat/get-stat-detail.php', {params: objData});
    let resData = result.data.data;
    let dataDetails = document.getElementById('data-details')
    let strTable = ''
    dataDetails.innerHTML = ''
    if(resData.length > 0){
      for(let i=0; i<resData.length; i++){
        strTable +=   `
        <div class="table">
          <div class="number">
          ${i+1}
          </div>
          <div class="answer" style="word-wrap: break-word;">
            ${resData[i]}
          </div>
        </div>
        `
      }
      dataDetails.innerHTML = strTable
    }
    hideLoading()
  }
  catch(err){
    // console.log(err)
  }
}

getProvinceTable()