<?php
require("connectdb.php");

$data = $_GET['data'];
$val = $_GET['val'];

if ($data == 'province') {
     echo "<select class='form-control' name='province' onChange=\"dochange('amphur', this.value)\">";
     echo "<option value=''>- เลือกจังหวัด -</option>\n";
     $result = $conn->query("SELECT * FROM province ORDER BY PROVINCE_NAME ASC");
     while ($row = $result->fetch_assoc()) {
          echo "<option value=\"$row[PROVINCE_ID]\" >$row[PROVINCE_NAME]</option> ";
     }
} else if ($data == 'amphur') {
     echo "<select class='form-control' name='amphur' onChange=\"dochange('district', this.value)\">";
     echo "<option value=''>- เลือกอำเภอ -</option>\n";
     $result = $conn->query("SELECT * FROM amphur WHERE PROVINCE_ID = $val ORDER BY AMPHUR_ID ASC");
     while ($row = $result->fetch_assoc()) {
          echo "<option value=\"$row[AMPHUR_ID]\" >$row[AMPHUR_NAME]</option> ";
     }
} else if ($data == 'district') {
     echo "<select class='form-control' name='district'>\n";
     echo "<option value=''>- เลือกตำบล -</option>\n";
     $result = $conn->query("SELECT * FROM district WHERE AMPHUR_ID = '$val' ORDER BY DISTRICT_ID ASC");
     while ($row = $result->fetch_assoc()) {
          echo "<option value=\"$row[DISTRICT_ID]\" >$row[DISTRICT_NAME]</option> \n";
     }
}
echo "</select>\n";
