<?php
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  require("connectdb.php");
  
  $s2_1_makeplan_operate = $_POST["s2_1_makeplan_operate"];
  $s2_1_how_or_because = $_POST["s2_1_how_or_because"];
  $s2_2_use_informationsystem = $_POST["s2_2_use_informationsystem"];
  $s2_2_how_or_because = $_POST["s2_2_how_or_because"];
  $s2_3_structure_management = $_POST["s2_3_structure_management"];
  $s2_3_how_or_because = $_POST["s2_3_how_or_because"];
  $s2_4_personnel_services_target = $_POST["s2_4_personnel_services_target"];
  $s2_4_how_or_because = $_POST["s2_4_how_or_because"];
  $s2_5_doproject_getsupport = $_POST["s2_5_doproject_getsupport"];
  $s2_5_agencyfrom_or_because = $_POST["s2_5_agencyfrom_or_because"];
  $s2_6_fixproblem_5dimention = $_POST["s2_6_fixproblem_5dimention"];
  $s2_6_how_or_because = $_POST["s2_6_how_or_because"];
  $s2_7_digital_contact = $_POST["s2_7_digital_contact"];
  $s2_7_how_or_because = $_POST["s2_7_how_or_because"];

  $sql_insert_sec_2 = "INSERT INTO `section2_mechanism_management`(`s2_1_makeplan_operate`, `s2_1_how_or_because`, `s2_2_use_informationsystem`, `s2_2_how_or_because`, `s2_3_structure_management`, `s2_3_how_or_because`, `s2_4_personnel_services_target`, `s2_4_how_or_because`, `s2_5_doproject_getsupport`, `s2_5_agencyfrom_or_because`, `s2_6_fixproblem_5dimention`, `s2_6_how_or_because`, `s2_7_digital_contact`, `s2_7_how_or_because`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
  $statement = $conn->prepare($sql_insert_sec_2);
  $statement->bind_param('ssssssssssssss', $s2_1_makeplan_operate, $s2_1_how_or_because, $s2_2_use_informationsystem, $s2_2_how_or_because, $s2_3_structure_management, $s2_3_how_or_because, $s2_4_personnel_services_target, $s2_4_how_or_because, $s2_5_doproject_getsupport, $s2_5_agencyfrom_or_because, $s2_6_fixproblem_5dimention, $s2_6_how_or_because, $s2_7_digital_contact, $s2_7_how_or_because);
  $statement->execute();
  $last_id = $conn->insert_id;

  $form_id = $_SESSION["form_id"];
  $sql_update_form = "UPDATE `form` SET `section2_id` = ? WHERE form_id = ?";
  $statement = $conn->prepare($sql_update_form);
  $statement->bind_param('ss', $last_id, $form_id);
  $statement->execute();

  header("Location: index3.php");
}