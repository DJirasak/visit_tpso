<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="assets/logo_icon.png">
    <title>แบบสอบถามการดำเนินงานของศูนย์ช่วยเหลือสังคมตำบล</title>
    <link rel="stylesheet" href="./global/css/style-global.css?version=1.0.2" />
    <link rel="stylesheet" href="./assets/css/style-index.css?version=1.0.7" />
</head>

<body>
    <form method="post" action="insert3.php">
        <section id="content">
            <div class="container-login">
                <div class="login-btn" onclick="gotoStart()">
                    หน้าหลัก
                </div>
            </div>
            <div class="logo">
                <img src="./assets/logo.png" />
            </div>
            <div class="title">
                แบบสอบถามการดำเนินงานของศูนย์ช่วยเหลือสังคมตำบล
            </div>
            <div class="sub-title">
                (การนิเทศงานของสำนักงานส่งเสริมและสนับสนุนวิชาการ 1-11)
            </div>
            <div class="sec-title" id="secTitle">
                ส่วนที่ 3/6
            </div>


            <section class="section" id="formSection">
                <div class="header">
                    ส่วนที่ 3 ด้านสถานที่ของศูนย์ช่วยเหลือสังคมตำบล
                </div>
                <div class="explain">
                    ในส่วนนี้เป็นคำถามที่ต้องการคำตอบในการจัดสภาพแวดล้อมทั้งภายในและภายนอกของศูนย์ช่วยเหลือสังคมตำบล เช่น
                    อาคารสถานที่ ขนาดพื้นที่ เป็นต้น
                </div>
                <div class="form">
                    <div class="question">
                        3.1 อาคารสถานที่ตั้งเหมาะสม (อาคารสถานที่ตั้งในตำแหน่งที่เหมาะสม ประชาชนเดินทางเข้าถึงได้สะดวก)
                    </div>
                    <div class="answer">
                        <div class="radio-box">
                            <div class="segment">
                                <input type="radio" id="s3_1_place_check_true" name="s3_1_place_check" onchange="selectRadio('s3_1_good_bad_place_because_1', 's3_1_good_bad_place_because_2')" value="เหมาะสม" required>
                                <label for="s3_1_place_check_true" class="left">เหมาะสม</label>
                                <input type="text" class="textInp" id="s3_1_good_bad_place_because_1" placeholder="โปรดระบุ" name="s3_1_bad_place_because">
                            </div>
                            <div class="segment">
                                <input type="radio" id="s3_1_place_check_false" name="s3_1_place_check" onchange="selectRadio('s3_1_good_bad_place_because_2', 's3_1_good_bad_place_because_1')" value="ไม่เหมาะสม เนื่องจาก">
                                <label for="s3_1_place_check_false" class="left">ไม่เหมาะสม เนื่องจาก</label>
                                <input type="text" class="textInp" id="s3_1_good_bad_place_because_2" placeholder="โปรดระบุ" name="s3_1_bad_place_because">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="question">
                        3.2 มีพื้นที่เพียงพอต่อการจัดกิจกรรม
                    </div>
                    <div class="answer">
                        <div class="radio-box">
                            <div class="segment">
                                <input type="radio" id="s3_2_area_check_true" name="s3_2_area_check" onchange="selectRadio('s3_2_good_bad_area_check_1', 's3_2_good_bad_area_check_2')" value="เพียงพอ" required>
                                <label for="s3_2_area_check_true" class="left">เพียงพอ</label>
                                <input type="text" class="textInp" id="s3_2_good_bad_area_check_1" placeholder="โปรดระบุ" name="s3_2_good_bad_area_check">
                            </div>
                            <div class="segment">
                                <input type="radio" id="s3_2_area_check_false" name="s3_2_area_check" onchange="selectRadio('s3_2_good_bad_area_check_2', 's3_2_good_bad_area_check_1')" value="ไม่เพียงพอ">
                                <label for="s3_2_area_check_false" class="left">ไม่เพียงพอ</label>
                                <input type="text" class="textInp" id="s3_2_good_bad_area_check_2" placeholder="โปรดระบุ" name="s3_2_good_bad_area_check">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="question">
                        3.3 มีห้องน้ำสะอาด และเพียงพอ
                    </div>
                    <div class="answer">
                        <div class="radio-box">
                            <div class="segment">
                                <input type="radio" id="s3_3_toilet_check_true" name="s3_3_toilet_check" onchange="selectRadio('s3_3_good_bad_toilet_check_1', 's3_3_good_bad_toilet_check_2')" value="เพียงพอ" required>
                                <label for="s3_3_toilet_check_true" class="left">เพียงพอ</label>
                                <input type="text" class="textInp" id="s3_3_good_bad_toilet_check_1" placeholder="โปรดระบุ" name="s3_3_good_bad_toilet_check">
                            </div>
                            <div class="segment">
                                <input type="radio" id="s3_3_toilet_check_false" name="s3_3_toilet_check" onchange="selectRadio('s3_3_good_bad_toilet_check_2', 's3_3_good_bad_toilet_check_1')" value="ไม่เพียงพอ">
                                <label for="s3_3_toilet_check_false" class="left">ไม่เพียงพอ</label>
                                <input type="text" class="textInp" id="s3_3_good_bad_toilet_check_2" placeholder="โปรดระบุ" name="s3_3_good_bad_toilet_check">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="question">
                        3.4 การจัดสิ่งอำนวยความสะดวกในอาคารที่เหมาะสมสำหรับกลุ่มเป้าหมาย
                    </div>
                    <div class="answer">
                        <div class="radio-box">
                            <div class="segment">
                                <input type="radio" id="s3_4_facility_check_true" name="s3_4_facility_check" onchange="selectRadio('s3_4_good_bad_facility_because_1', 's3_4_good_bad_facility_because_2')" value="เหมาะสม" required>
                                <label for="s3_4_facility_check_true" class="left">เหมาะสม</label>
                                <input type="text" class="textInp" id="s3_4_good_bad_facility_because_1" placeholder="โปรดระบุ" name="s3_4_good_bad_facility_because">
                            </div>
                            <div class="segment">
                                <input type="radio" id="s3_4_facility_check_false" name="s3_4_facility_check" onchange="selectRadio('s3_4_good_bad_facility_because_2', 's3_4_good_bad_facility_because_1')" value="ไม่เหมาะสม เนื่องจาก">
                                <label for="s3_4_facility_check_false" class="left">ไม่เหมาะสม เนื่องจาก</label>
                                <input type="text" class="textInp" id="s3_4_good_bad_facility_because_2" placeholder="โปรดระบุ" name="s3_4_good_bad_facility_because">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-state" id="container-state">
                <input type="button" class="next-btn" style="width:15vmax;" value="ย้อนกลับ" onclick="history.back()">
                <input type="submit" class="next-btn" style="width:15vmax;" value="ถัดไป">
            </div>

        </section>
    </form>

    <script src="./global/js/global.js?version=1.0.2"></script>
    <script src="./assets/js/script-index.js?version=1.0.6"></script>
</body>

</html>