<?php
session_start();

// remove all session variables
session_unset();

// destroy the session
session_destroy();

echo "<script>
alert('บันทึกสำเร็จ');
window.location.href='index.php';
</script>";
