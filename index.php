<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="assets/logo_icon.png">
    <title>แบบสอบถามการดำเนินงานของศูนย์ช่วยเหลือสังคมตำบล</title>
    <link rel="stylesheet" href="./global/css/style-global.css?version=1.0.2" />
    <link rel="stylesheet" href="./assets/css/style-index.css?version=1.0.7" />

    <script language=Javascript>
        function Inint_AJAX() {
            try {
                return new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {} //IE
            try {
                return new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {} //IE
            try {
                return new XMLHttpRequest();
            } catch (e) {} //Native Javascript
            alert("XMLHttpRequest not supported");
            return null;
        };

        function dochange(src, val) {
            var req = Inint_AJAX();
            req.onreadystatechange = function() {
                if (req.readyState == 4) {
                    if (req.status == 200) {
                        document.getElementById(src).innerHTML = req.responseText; //รับค่ากลับมา
                    }
                }
            };
            req.open("GET", "localtion.php?data=" + src + "&val=" + val); //สร้าง connection
            req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8"); // set Header
            req.send(null); //ส่งค่า
        }

        window.onLoad = dochange('province', -1);
    </script>
</head>

<body>
    <form method="post" action="insert1.php">
        <section id="content">
            <div class="container-login">
                <div class="login-btn" onclick="gotoSignin()">
                    สำหรับเจ้าหน้าที่
                </div>
            </div>
            <div class="logo">
                <img src="./assets/logo.png" />
            </div>
            <div class="title">
                แบบสอบถามการดำเนินงานของศูนย์ช่วยเหลือสังคมตำบล
            </div>
            <div class="sub-title">
                (การนิเทศงานของสำนักงานส่งเสริมและสนับสนุนวิชาการ 1-11)
            </div>
            <div class="sec-title" id="secTitle">
                ส่วนที่ 1/6
            </div>

            <section class="section" id="formSection">
                <div class="header">
                    ส่วนที่ 1 ข้อมูลทั่วไป
                </div>
                <div class="form">
                    <div class="question">
                        1.1 ผู้ให้ข้อมูล
                    </div>
                    <div class="answer">
                        <p>1.1.1 กรุณาเลือกจังหวัด </p>
                        <span id="province">
                            <select name="province" id="province" class="form-control" required>
                                <option value=''>- เลือกจังหวัด -</option>
                            </select>
                        </span>

                        <p>1.1.2 กรุณาเลือกอำเภอ</p>
                        <span id="amphur">
                            <select name="amphur" id="amphur" class="form-control" required>
                                <option value=''>- เลือกอำเภอ -</option>
                            </select>
                        </span>

                        <p>1.1.3 กรุณาเลือกตำบล</p>
                        <span id="district">
                            <select name="district" id="district" class="form-control" required>
                                <option value=''>- เลือกตำบล -</option>
                            </select>
                        </span>


                        <p>1.1.4 ตำแหน่ง</p>
                        <div class="radio-box">
                            <div class="segment">
                                <input type="radio" id="s1_2_information_1" name="s1_2_information" value="คณะทำงาน/คณะกรรมการศูนย์ช่วยเหลือสังคมตำบล" required>
                                <label for="s1_2_information_1">คณะทำงาน/คณะกรรมการศูนย์ช่วยเหลือสังคมตำบล</label>
                            </div>
                            <div class="segment">
                                <input type="radio" id="s1_2_information_2" name="s1_2_information" value="อาสาสมัครพัฒนาสังคมและความมั่นคงของมนุษย์" required>
                                <label for="s1_2_information_2">อาสาสมัครพัฒนาสังคมและความมั่นคงของมนุษย์</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form">
                    <div class="question">
                        1.2 บทบาทของท่านในการขับเคลื่อนศูนย์ช่วยเหลือสังคมตำบล (เลือกได้มากกว่า 1 คำตอบ)
                    </div>
                    <div class="answer">
                        <div class="radio-box">
                            <div class="segment">
                                <input type="checkbox" id="s1_3_role_a" name="s1_3_role[]" value="ผู้กำหนดนโยบาย">
                                <label for="s1_3_role_a">ผู้กำหนดนโยบายในการขับเคลื่อนศูนย์ช่วยเหลือสังคมตำบล</label>
                            </div>
                            <div class="segment">
                                <input type="checkbox" id="s1_3_role_b" name="s1_3_role[]" value="ผู้ปฏิบัติ">
                                <label for="s1_3_role_b">ผู้มีส่วนในการปฏิบัติงานในศูนย์ช่วยเหลือสังคมตำบล</label>
                            </div>
                            <div class="segment">
                                <input type="checkbox" id="s1_3_role_c" name="s1_3_role[]" value="ผู้ประสานงาน">
                                <label for="s1_3_role_c">ผู้ประสานงานระหว่างหน่วยงานต่าง ๆ กับศูนย์ช่วยเหลือสังคมตำบล</label>
                            </div>
                            <div class="segment">
                                <input type="checkbox" id="s1_3_role_d" name="s1_3_role[]" value="อื่นๆ">
                                <label for="s1_3_role_d">อื่น ๆ โปรดระบุ</label>
                                <input type="text" class="textInp-1" id="s1_3_other_role" placeholder="โปรดระบุ" name="s1_3_other_role">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-state" id="container-state">
                <input type="submit" class="next-btn" style="width:15vmax;" value="ถัดไป">
            </div>
        </section>
    </form>

    <script src="./global/js/global.js?version=1.0.2"></script>
    <script src="./assets/js/script-index.js?version=1.0.6"></script>
</body>

</html>