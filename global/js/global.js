var JSON_DATA  = {}
var PROVINCE_ARR = []
const readJson = async () => {
    const response = await fetch('./global/json/thailand_address.json')
    JSON_DATA = await response.clone().json()
}

const getProvince = () => {
    let data = []
    for (let i = 0; i < JSON_DATA.length; i++) {
        data.push(JSON_DATA[i].province);
    }
    uniqueArray = data.filter(function(item, pos) {
        return data.indexOf(item) == pos;
    })
    uniqueArray.sort()
    return uniqueArray
}

const getDistrict = (inpProvince) => {
    let data = []
    for (let i = 0; i < JSON_DATA.length; i++) {
        if(JSON_DATA[i].province === inpProvince){
            data.push(JSON_DATA[i].district);
        }
    }
    uniqueArray = data.filter(function(item, pos) {
        return data.indexOf(item) == pos;
    })
    return uniqueArray
}

const getSubDistrict = (inpProvince, inpDistrict) => {
    let data = []
    for (let i = 0; i < JSON_DATA.length; i++) {
        if(JSON_DATA[i].province === inpProvince && JSON_DATA[i].district === inpDistrict){
            data.push(JSON_DATA[i].subdistrict);
        }
    }
    uniqueArray = data.filter(function(item, pos) {
        return data.indexOf(item) == pos;
    })
    return uniqueArray
}
const run = async ()=>{
    await readJson()
    PROVINCE_ARR = getProvince()
}

function showLoading(){
    document.getElementById('loading').style.display = "block";
}

function hideLoading(){
    document.getElementById('loading').style.display = "none";
}

