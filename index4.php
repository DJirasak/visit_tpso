<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="assets/logo_icon.png">
    <title>แบบสอบถามการดำเนินงานของศูนย์ช่วยเหลือสังคมตำบล</title>
    <link rel="stylesheet" href="./global/css/style-global.css?version=1.0.2" />
    <link rel="stylesheet" href="./assets/css/style-index.css?version=1.0.6" />
</head>

<body>
    <div class="container">
        <form method="post" action="insert4.php">
            <section id="content">
                <div class="container-login">
                    <div class="login-btn" onclick="gotoStart()">
                        หน้าหลัก
                    </div>
                </div>
                <div class="logo">
                    <img src="./assets/logo.png" />
                </div>
                <div class="title">
                    แบบสอบถามการดำเนินงานของศูนย์ช่วยเหลือสังคมตำบล
                </div>
                <div class="sub-title">
                    (การนิเทศงานของสำนักงานส่งเสริมและสนับสนุนวิชาการ 1-11)
                </div>
                <div class="sec-title" id="secTitle">
                    ส่วนที่ 4/6
                </div>


                <section class="section" id="formSection">

                    <div class="header">
                        ส่วนที่ 4 ด้านการใช้ข้อมูลและการให้บริการ
                    </div>
                    <div class="explain">
                        ในส่วนนี้เป็นคำถามที่ต้องการคำตอบในการใช้ข้อมูล TPMAP
                        การให้บริการตรงกับความต้องการของกลุ่มเป้าหมายได้อย่างตรงจุดและรวดเร็ว
                        กลุ่มเป้าหมายได้รับการบิการอย่างมีประสิทธิภาพมากขึ้น
                    </div>
                    <div class="form">
                        <div class="question">
                            4.1 มีการนำข้อมูล TPMAP มาวิเคราะห์
                            และสรุปผลเพื่อนำไปสู่การวางแผนงานพัฒนาคุณภาพชีวิตกลุ่มเปราะบางรายครัวเรือน ในมิติ 5 มิติ
                            ประกอบไปด้วย 1) การเข้าถึงบริการภาครัฐ 2) รายได้ 3) สุขภาพ
                        </div>
                        <div class="answer">
                            <div class="radio-box">
                                <div class="segment">
                                    <input type="radio" id="s4_1_tpmap_conclude_true" name="s4_1_tpmap_conclude" onchange="selectRadio('s4_1_how_or_because_1', 's4_1_how_or_because_2')" value="มี อย่างไร" required>
                                    <label for="s4_1_tpmap_conclude_true" class="left">มี อย่างไร </label>
                                    <input type="text" class="textInp" id="s4_1_how_or_because_1" placeholder="โปรดระบุ" name="s4_1_how_or_because">
                                </div>
                                <div class="segment">
                                    <input type="radio" id="s4_1_tpmap_conclude_false" name="s4_1_tpmap_conclude" onchange="selectRadio('s4_1_how_or_because_2', 's4_1_how_or_because_1')" value="ไม่มี เนื่องจาก">
                                    <label for="s4_1_tpmap_conclude_false" class="left">ไม่มี เนื่องจาก</label>
                                    <input type="text" class="textInp" id="s4_1_how_or_because_2" placeholder="โปรดระบุ" name="s4_1_how_or_because">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form">
                        <div class="question">
                            4.2 มีการนำข้อมูลมาสรุป วิเคราะห์ และกำหนดสถานการณ์ปัญหาทางสังคมในพื้นที่อย่างไร
                            และปัญหาใดเป็นปัญหาสำคัญที่ต้องเร่งดำเนินการ
                        </div>
                        <div class="answer">
                            <div class="radio-box">
                                <div class="segment">
                                    <input type="radio" id="s4_2_tpmap_analyze_true" name="s4_2_tpmap_analyze" onchange="selectRadio('s4_2_how_or_because_1', 's4_2_how_or_because_2', 's4_2_how_or_because_3')" value="มี อย่างไร" required>
                                    <label for="s4_2_tpmap_analyze_true" class="left">มี อย่างไร</label>
                                    <input type="text" class="textInp" id="s4_2_how_or_because_1" placeholder="โปรดระบุ" name="s4_2_how_or_because">
                                </div>
                                <div class="segment">
                                    <input type="radio" id="s4_2_tpmap_analyze_false" name="s4_2_tpmap_analyze" onchange="selectRadio('s4_2_how_or_because_2', 's4_2_how_or_because_1', 's4_2_how_or_because_3')" value="ไม่มี เนื่องจาก">
                                    <label for="s4_2_tpmap_analyze_false" class="left">ไม่มี เนื่องจาก</label>
                                    <input type="text" class="textInp" id="s4_2_how_or_because_2" placeholder="โปรดระบุ" name="s4_2_how_or_because">
                                </div>
                                <div class="segment">
                                    <input type="radio" id="s4_2_tpmap_analyze_select" name="s4_2_tpmap_analyze" onchange="selectRadio('s4_2_how_or_because_3', 's4_2_how_or_because_1', 's4_2_how_or_because_2')" value="ประเด็นปัญหาทางสังคมที่สำคัญที่ต้องเร่งดำเนินการ มีปัญหาใดบ้างเรียงลำดับ">
                                    <label for="s4_2_tpmap_analyze_select">ประเด็นปัญหาทางสังคมที่สำคัญที่ต้องเร่งดำเนินการ
                                        มีปัญหาใดบ้างเรียงลำดับ</label>
                                    <!-- <input type="text" class="textInp" id="s4_2_how_or_because_3" placeholder="โปรดระบุ" name="s4_2_how_or_because"> -->
                                </div>
                                <textarea placeholder="โปรดระบุ เป็นข้อ" id="s4_2_how_or_because_3" name="s4_2_how_or_because" style="display: none;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form">
                        <div class="question">
                            4.3 การจัดทำแผนพัฒนาคุณภาพชีวิตกลุ่มเปราะบางรายครัวเรือน ดำเนินการอย่างไร
                        </div>
                        <div class="answer">
                            <div class="radio-box">
                                <div class="segment">
                                    <input type="radio" id="s4_3_tpmap_plan_true" name="s4_3_tpmap_plan" onchange="selectRadio('s4_3_how_or_because_1', 's4_3_how_or_because_2')" value="มี อย่างไร">
                                    <label for="s4_3_tpmap_plan_true" class="left">มี อย่างไร</label>
                                    <input type="text" class="textInp" id="s4_3_how_or_because_1" placeholder="โปรดระบุ" name="s4_3_how_or_because">
                                </div>
                                <div class="segment">
                                    <input type="radio" id="s4_3_tpmap_plan_false" name="s4_3_tpmap_plan" onchange="selectRadio('s4_3_how_or_because_2', 's4_3_how_or_because_1')" value="ไม่มี เนื่องจาก">
                                    <label for="s4_3_tpmap_plan_false" class="left">ไม่มี เนื่องจาก</label>
                                    <input type="text" class="textInp" id="s4_3_how_or_because_2" placeholder="โปรดระบุ" name="s4_3_how_or_because">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form">
                        <div class="question">
                            4.4 การนำประเด็นสถานการณ์ทางสังคมที่ได้ร่วมกันวิเคราะห์ มาจัดทำแผนป้องกัน แก้ไขปัญหา และพัฒนา
                            ดำเนินการอย่างไร
                        </div>
                        <div class="answer">
                            <div class="radio-box">
                                <div class="segment">
                                    <input type="radio" id="s4_4_tpmap_protect_true" name="s4_4_tpmap_protect" onchange="selectRadio('s4_4_how_or_because_1', 's4_4_how_or_because_2')" value="มี อย่างไร">
                                    <label for="s4_4_tpmap_protect_true" class="left">มี อย่างไร</label>
                                    <input type="text" class="textInp" id="s4_4_how_or_because_1" placeholder="โปรดระบุ" name="s4_4_how_or_because">
                                </div>
                                <div class="segment">
                                    <input type="radio" id="s4_4_tpmap_protect_false" name="s4_4_tpmap_protect" onchange="selectRadio('s4_4_how_or_because_2', 's4_4_how_or_because_1')" value="ไม่มี เนื่องจาก">
                                    <label for="s4_4_tpmap_protect_false" class="left">ไม่มี เนื่องจาก</label>
                                    <input type="text" class="textInp" id="s4_4_how_or_because_2" placeholder="โปรดระบุ" name="s4_4_how_or_because">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form">
                        <div class="question">
                            4.5 การจัดบริการให้แก่กลุ่มเป้าหมายได้อย่างทั่วถึง
                        </div>
                        <div class="answer">
                            <div class="radio-box">
                                <div class="segment">
                                    <input type="radio" id="s4_5_thorough_service_true" name="s4_5_thorough_service" onchange="selectRadio('s4_5_no_thorough_because_1', 's4_5_no_thorough_because_2')" value="ทั่วถึง">
                                    <label for="s4_5_thorough_service_true" class="left">ทั่วถึง</label>
                                    <input type="text" class="textInp" id="s4_5_no_thorough_because_1" placeholder="โปรดระบุ" name="s4_5_no_thorough_because">
                                </div>
                                <div class="segment">
                                    <input type="radio" id="s4_5_thorough_service_false" name="s4_5_thorough_service" onchange="selectRadio('s4_5_no_thorough_because_2', 's4_5_no_thorough_because_1')" value="ไม่ทั่วถึง เนื่องจาก">
                                    <label for="s4_5_thorough_service_false" class="left">ไม่ทั่วถึง เนื่องจาก</label>
                                    <input type="text" class="textInp" id="s4_5_no_thorough_because_2" placeholder="โปรดระบุ" name="s4_5_no_thorough_because">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form">
                        <div class="question">
                            4.6 มีการจัดบริการได้ตรงตามความต้องการของกลุ่มเป้าหมาย
                        </div>
                        <div class="answer">
                            <div class="radio-box">
                                <div class="segment">
                                    <input type="radio" id="s4_6_goal_service_true" name="s4_6_goal_service" onchange="selectRadio('s4_6_goal_bad_because_1', 's4_6_goal_bad_because_2')" value="ตรงตามต้องการ">
                                    <label for="s4_6_goal_service_true" class="left">ตรงตามต้องการ</label>
                                    <input type="text" class="textInp" id="s4_6_goal_bad_because_1" placeholder="โปรดระบุ" name="s4_6_goal_bad_because">
                                </div>
                                <div class="segment">
                                    <input type="radio" id="s4_6_goal_service_false" name="s4_6_goal_service" onchange="selectRadio('s4_6_goal_bad_because_2', 's4_6_goal_bad_because_1')" value="ไม่ตรงตามต้องการ เนื่องจาก">
                                    <label for="s4_6_goal_service_false" class="left">ไม่ตรงตามต้องการ เนื่องจาก</label>
                                    <input type="text" class="textInp" id="s4_6_goal_bad_because_2" placeholder="โปรดระบุ" name="s4_6_goal_bad_because">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form">
                        <div class="question">
                            4.7 มีการจัดบริการและจัดกิจกรรม อะไรบ้าง
                        </div>
                        <div class="answer">
                            <div class="radio-box">
                                <div class="segment">
                                    <label>1) การเข้าถึงบริการภาครัฐ</label>
                                </div>
                            </div>
                            <div class="textarea-box">
                                <textarea placeholder="โปรดระบุ" class="textarea7" name="s4_7_1_access_service" id="textarea_4_7_1"></textarea>
                            </div>
                            <div class="radio-box">
                                <div class="segment">
                                    <label>2) รายได้</label>
                                </div>
                            </div>
                            <div class="textarea-box">
                                <textarea placeholder="โปรดระบุ" class="textarea7" name="s4_7_2_income" id="textarea_4_7_2"></textarea>
                            </div>
                            <div class="radio-box">
                                <div class="segment">
                                    <label>3) สุขภาพ </label>
                                </div>
                            </div>
                            <div class="textarea-box">
                                <textarea placeholder="โปรดระบุ" class="textarea7" name="s4_7_3_healt" id="textarea_4_7_3"></textarea>
                            </div>
                            <div class="radio-box">
                                <div class="segment">
                                    <label>4) การศึกษา </label>
                                </div>
                            </div>
                            <div class="textarea-box">
                                <textarea placeholder="โปรดระบุ" class="textarea7" name="s4_7_4_education" id="textarea_4_7_4"></textarea>
                            </div>
                            <div class="radio-box">
                                <div class="segment">
                                    <label>5) ความเป็นอยู่</label>
                                </div>
                            </div>
                            <div class="textarea-box">
                                <textarea placeholder="โปรดระบุ" class="textarea7" name="s4_7_5_being" id="textarea_4_7_5"></textarea>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="container-state" id="container-state">
                    <input type="button" class="next-btn" style="width:15vmax;" value="ย้อนกลับ" onclick="history.back()">
                    <input type="submit" class="next-btn" style="width:15vmax;" value="ถัดไป">
                </div>

            </section>
        </form>

        <script src="./global/js/global.js?version=1.0.2"></script>
        <script src="./assets/js/script-index.js?version=1.0.6"></script>
    </div>
</body>

</html>