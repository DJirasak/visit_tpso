<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="assets/logo_icon.png">
    <title>แบบสอบถามการดำเนินงานของศูนย์ช่วยเหลือสังคมตำบล</title>
    <link rel="stylesheet" href="./global/css/style-global.css?version=1.0.2" />
    <link rel="stylesheet" href="./assets/css/style-index.css?version=1.0.6" />
</head>

<body>
    <div class="container">
        <form method="post" action="insert6.php">
            <section id="content">
                <div class="container-login">
                    <div class="login-btn" onclick="gotoStart()">
                        หน้าหลัก
                    </div>
                </div>
                <div class="logo">
                    <img src="./assets/logo.png" />
                </div>
                <div class="title">
                    แบบสอบถามการดำเนินงานของศูนย์ช่วยเหลือสังคมตำบล
                </div>
                <div class="sub-title">
                    (การนิเทศงานของสำนักงานส่งเสริมและสนับสนุนวิชาการ 1-11)
                </div>
                <div class="sec-title" id="secTitle">
                    ส่วนที่ 6/6
                </div>

                <section class="section" id="formSection">
                    <div class="header">
                        ส่วนที่ 6 ด้านปัญหา อุปสรรค และข้อเสนอแนะ
                    </div>
                    <div class="form">
                        <div class="question">
                            6.1 ปัญหา อุปสรรค
                        </div>
                        <div class="answer">
                            <div class="radio-box">
                                <div class="segment">
                                    <label>1) ด้านกลไกและการบริหารจัดการ</label>
                                </div>
                            </div>
                            <div class="textarea-box">
                                <textarea placeholder="โปรดระบุ" name="s6_1_1_problem_management" id="textarea_6_1_1" class="textarea7"></textarea>
                            </div>
                            <div class="radio-box">
                                <div class="segment">
                                    <label>2) ด้านสถานที่ของศูนย์ช่วยเหลือสังคมตำบล</label>
                                </div>
                            </div>
                            <div class="textarea-box">
                                <textarea placeholder="โปรดระบุ" name="s6_1_2_problem_place" id="textarea_6_1_2" class="textarea7"></textarea>
                            </div>
                            <div class="radio-box">
                                <div class="segment">
                                    <label>3) ด้านการใช้ข้อมูลและการให้บริการ</label>
                                </div>
                            </div>
                            <div class="textarea-box">
                                <textarea placeholder="โปรดระบุ" name="s6_1_3_problem_service" id="textarea_6_1_3" class="textarea7"></textarea>
                            </div>
                            <div class="radio-box">
                                <div class="segment">
                                    <label>4) ด้านการบูรณาการความร่วมมือ</label>
                                </div>
                            </div>
                            <div class="textarea-box">
                                <textarea placeholder="โปรดระบุ" name="s6_1_4_problem_integration" id="textarea_6_1_4" class="textarea7"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form">
                        <div class="question">
                            6.2 ข้อเสนอแนะ
                        </div>
                        <div class="answer">
                            <div class="radio-box">
                                <div class="segment">
                                    <label>1) ด้านกลไกและการบริหารจัดการ</label>
                                </div>
                            </div>
                            <div class="textarea-box">
                                <textarea placeholder="โปรดระบุ" name="s6_2_1_offer_management" id="textarea_6_2_1" class="textarea7"></textarea>
                            </div>
                            <div class="radio-box">
                                <div class="segment">
                                    <label>2) ด้านสถานที่ของศูนย์ช่วยเหลือสังคมตำบล</label>
                                </div>
                            </div>
                            <div class="textarea-box">
                                <textarea placeholder="โปรดระบุ" name="s6_2_2_offer_place" id="textarea_6_2_2" class="textarea7"></textarea>
                            </div>
                            <div class="radio-box">
                                <div class="segment">
                                    <label>3) ด้านการใช้ข้อมูลและการให้บริการ</label>
                                </div>
                            </div>
                            <div class="textarea-box">
                                <textarea placeholder="โปรดระบุ" name="s6_2_3_offer_service" id="textarea_6_2_3" class="textarea7"></textarea>
                            </div>
                            <div class="radio-box">
                                <div class="segment">
                                    <label>4) ด้านการบูรณาการความร่วมมือ</label>
                                </div>
                            </div>
                            <div class="textarea-box">
                                <textarea placeholder="โปรดระบุ" name="s6_2_4_offer_management" id="textarea_6_2_4" class="textarea7"></textarea>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="container-state" id="container-state">
                    <input type="button" class="next-btn" style="width:15vmax;" value="ย้อนกลับ" onclick="history.back()">
                    <input type="submit" class="next-btn" style="width:15vmax;" value="ถัดไป">
                </div>

            </section>
        </form>

        <script src="./global/js/global.js?version=1.0.2"></script>
        <script src="./assets/js/script-index.js?version=1.0.6"></script>

    </div>

    <br>




    </div>
</body>

</html>