let userData = JSON.parse(localStorage.getItem("objLocal"));
// console.log(userData)

if(userData == null){
  window.open('../','_self')
}
else{
    if(userData.role == 'admin' || userData.role == 'user'){
        document.getElementById('usernameShow').innerHTML = userData.username;
    }
    else{
        window.open('../','_self')
    }
}

const gotoHome =() =>{
    window.open('../dashboard','_self')
}

const gotoSignIn =()=> {
    localStorage.removeItem('objLocal');
    window.open('../','_self')
}

const checkPassword =()=>{
    
    let password = document.getElementById('passwordChange').value
    document.getElementById('errEdit').innerHTML = ''
    if(password.length >= 8){
        document.getElementById('length8_check').style.color = 'green'
    }
    else{
        document.getElementById('length8_check').style.color = 'black'
        document.getElementById('lower_check').style.color = 'black'
        document.getElementById('upper_check').style.color = 'black'
    }
    if(password.match(/[a-z]/)){
        document.getElementById('lower_check').style.color = 'green'
    }
    else{
        document.getElementById('length8_check').style.color = 'black'
        document.getElementById('lower_check').style.color = 'black'
        document.getElementById('upper_check').style.color = 'black'
    }
    if(password.match(/[A-Z]/)){
        document.getElementById('upper_check').style.color = 'green'
    }
    else{
        document.getElementById('length8_check').style.color = 'black'
        document.getElementById('lower_check').style.color = 'black'
        document.getElementById('upper_check').style.color = 'black'
    }
}

const checkCPassword =()=>{
    let password = document.getElementById('passwordChange').value
    let cpassword = document.getElementById('cpasswordChange').value

    if(password != cpassword){
        document.getElementById('errEdit').style.color = 'red'
        document.getElementById('errEdit').innerHTML = '*รหัสผ่านไม่ตรงกัน กรุณาลองใหม่อีกครั้ง'
    }
    else{
        document.getElementById('errEdit').innerHTML = ''
    }
}

const editPassword= async()=>{
    let oldpassword = document.getElementById('oldpassword').value
    let password = document.getElementById('passwordChange').value
    let cpassword = document.getElementById('cpasswordChange').value

    if(oldpassword == ''){
        document.getElementById('errEdit').style.color = 'red'
        document.getElementById('errEdit').innerHTML = '*กรุณากรอกรหัสผ่านปัจจุบัน'
    }
    else if(password.length < 8 || !password.match(/[a-z]/) || !password.match(/[A-Z]/)){
        document.getElementById('errEdit').style.color = 'red'
        document.getElementById('errEdit').innerHTML = '*รูปแบบของรหัสผ่านไม่ถูกต้อง'
    }
    else if(password != cpassword){
        document.getElementById('errEdit').style.color = 'red'
        document.getElementById('errEdit').innerHTML = '*รหัสผ่านไม่ตรงกัน กรุณาลองใหม่อีกครั้ง'
    }
    else{
        try{
            let objData = {
                username: btoa(userData.username),
                curr_password: btoa(oldpassword),
                new_password: btoa(password)
            }
            showLoading()
            let result = await axios.post('../services/account/reset-password.php', objData);
            document.getElementById('errEdit').innerHTML = ''
            alert('แก้ไขรหัสผ่านสำเร็จ !')
            location.reload()
            hideLoading()
        }
        catch(err){
            if(err.response.data.message == "Password Incorrect!"){
                alert('รหัสผ่านปัจจุบันไม่ถูกต้อง กรุณากรอกใหม่ !')
                location.reload()
            }
            hideLoading()
        }
    }
}