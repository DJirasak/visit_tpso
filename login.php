<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="icon" type="image/x-icon" href="./assets/logo_icon.png">
  <title>เข้าสู่ระบบ</title>
  <link rel="stylesheet" href="./global/css/style-global.css?version=1.0.2" />
  <link rel="stylesheet" href="./assets/css/style-signin.css?version=1.0.4">
</head>

<body>
<section id="loading">
    <div class="screen">
      <div class="loadPad">
        <div class="spinning">
          <div class="ldsSpinner">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="content-login">

    <div class="loginBox">

      <div class="loginTitle">
        เข้าสู่ระบบ
      </div>

      <form action="" method="post">

        <div class="inputArea">
          <div class="inputTitle">ชื่อผู้ใช้</div>
          <input placeholder="ชื่อผู้ใช้" id='username' type="text" />
          <div class="inputTitle">รหัสผ่าน</div>
          <input placeholder="รหัสผ่าน" id='password' type="password" />
        </div>

        <div class="errMsg" id='errLogin'></div>

        <div class="buttonArea">
          <div class="button" onclick="auth()">เข้าสู่ระบบ</div>
        </div>

      </form>

    </div>

  </section>

  <section class="background-login">
    <div class="area">
      <ul class="circles">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
    </div>
  </section>

  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  <script src="./global/js/global.js?version==1.0.7"></script>
  <script src="./assets/js/script-signin.js?version=1.1.7"></script>
</body>

</html>